#include <vector>
#include <string>

#include "gtest/gtest.h"
#include "HandCfg.h"

using namespace std;

class HandCfgTest : public ::testing::Test{
    public:
        HandCfgTest()
            :conf("../conf/handcfg.yaml"){};

    private:
        virtual void SetUp(){
        };

    protected:
        HandCfg conf;
};

/*
 * Verify read data with conf/handcfg.yaml
 *
 * */
TEST_F(HandCfgTest, VerifyRead){
    string                  f_tty_port          = "/dev/ttyUSB0";
    unsigned int            f_num_of_dofs       = 16;
    unsigned int            f_num_of_fingers    = 4;
    vector<unsigned int>    f_num_of_jPf{4,  4,  4,  4};
    vector<unsigned int>    f_j2sid{33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48};
    vector<double>          f_joint_home{0,  0,  0,  0, 0,  0,  0,  0, 0,  0,  0,  0, 0,  0,  0,  0};
    vector<double>          f_joint_speed{80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80};
    vector<unsigned char>   f_compliant_cw{80, 90, 120, 150, 80, 90, 120, 150, 80, 90, 120, 150, 80, 90, 120, 150};
    vector<unsigned char>   f_compliant_ccw{80, 90,  40,  40, 80, 40,  40,  40, 80, 40,  40,  40, 80, 40,  40,  40};

    EXPECT_TRUE(conf.tty_port           == f_tty_port);
    EXPECT_TRUE(conf.num_of_dofs        == f_num_of_dofs);
    EXPECT_TRUE(conf.num_of_fingers     == f_num_of_fingers);
    EXPECT_TRUE(conf.num_of_jPf         == f_num_of_jPf);
    EXPECT_TRUE(conf.j2sid              == f_j2sid);
    EXPECT_TRUE(conf.joint_home         == f_joint_home);
    EXPECT_TRUE(conf.joint_speed        == f_joint_speed);
    EXPECT_TRUE(conf.compliant_cw       == f_compliant_cw);
    EXPECT_TRUE(conf.compliant_ccw      == f_compliant_ccw);
}

