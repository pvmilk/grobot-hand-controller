#include <vector>

#include "gtest/gtest.h"
#include "RSInterface.h"

using namespace std;

class RSInterfaceTest : public ::testing::Test{
    virtual void SetUp(){
        ids = vector<unsigned int>(ids_temp, ids_temp+20);

        unsigned int mid = ids.size()/2;
        for(unsigned int k = 0; k < mid; k++){
            sub_ids1.push_back(ids[k]);
        }
        for(unsigned int k = mid; k < ids.size(); k++){
            sub_ids2.push_back(ids[k]);
        }

        EXPECT_EQ(ids.size(), sub_ids1.size()+sub_ids2.size());

        angle_near = 2.0;
    };

    protected :
        RSInterface rsi;
        static const unsigned int ids_temp[20];
        vector<unsigned int> ids;
        vector<unsigned int> sub_ids1;
        vector<unsigned int> sub_ids2;
        
        double angle_near; // in degree
};

const unsigned int RSInterfaceTest::ids_temp[20] =    {     80, 81, 82, 83,        /// F0
                                                            76, 77, 78, 79,        /// F1
                                                            72, 73, 74, 75,        /// F2
                                                            68, 69, 70, 71,        /// F3
                                                            64, 65, 66, 67  };     /// F4

// Turn on/off each servos and check their status
TEST_F(RSInterfaceTest, DISABLED_TorqueIndividual){
    for(unsigned int k=0; k < ids.size(); k++){
        unsigned int id = ids[k];

        //turn on
        rsi.torque_onoff(id, true);
        EXPECT_EQ( 1, rsi.torque_status(id));

        //turn off
        rsi.torque_onoff(id, false);
        EXPECT_EQ( 0, rsi.torque_status(id));
    }
};

// Turn on/off all servos and check their status
TEST_F(RSInterfaceTest, DISABLED_TorqueMultiple){

    // Turn on ALL
    rsi.torque_onoff(ids.size(), ids.data(), true);
    for(unsigned int k=0; k < ids.size(); k++){
        unsigned int id = ids[k];
        EXPECT_EQ(1, rsi.torque_status(id));
    }

    // Turn off first half
    rsi.torque_onoff(sub_ids1.size(), sub_ids1.data(), false);
    for(unsigned int k=0; k < sub_ids1.size(); k++){
        unsigned int id = sub_ids1[k];
        EXPECT_EQ(0, rsi.torque_status(id));
    }
    for(unsigned int k=0; k < sub_ids2.size(); k++){
        unsigned int id = sub_ids2[k];
        EXPECT_EQ(1, rsi.torque_status(id));
    }

    // Turn off second half
    rsi.torque_onoff(sub_ids2.size(), sub_ids2.data(), false);
    for(unsigned int k=0; k < sub_ids2.size(); k++){
        unsigned int id = sub_ids2[k];
        EXPECT_EQ(0, rsi.torque_status(id));
    }
};

TEST_F(RSInterfaceTest, DISABLED_RotateDirIndividual){
    // Turn on all servos
    rsi.torque_onoff(ids.size(), ids.data(), true);

    for(unsigned int k=0; k < ids.size(); k++){
        unsigned int id = ids[k];

        // Set Rotate dir to 'reverse'
        EXPECT_FALSE(rsi.set_rotate_dir(id, true));
        // Set rotate dir to 'normal'
        EXPECT_FALSE(rsi.set_rotate_dir(id, false));
    }

    // Turn off all servos and start setting
    rsi.torque_onoff(ids.size(), ids.data(), false);
    for(unsigned int k=0; k < ids.size(); k++){
        unsigned int id = ids[k];

        // Set rotate dir to 'reverse'
        EXPECT_TRUE(rsi.set_rotate_dir(id, true));
        // Check rotate dir as 'reverse'
        EXPECT_TRUE(rsi.getRotateDir(id));

        // Set rotate dir to 'normal'
        EXPECT_TRUE(rsi.set_rotate_dir(id, false));
        // Check rotate dir as 'normal'
        EXPECT_FALSE(rsi.getRotateDir(id));
    }
};

TEST_F(RSInterfaceTest, DISABLED_RotateDirMultiple){
    // Turn on first half of the servo
    // Turn off second half of the servo
    rsi.torque_onoff(sub_ids1.size(), sub_ids1.data(), true);
    rsi.torque_onoff(sub_ids2.size(), sub_ids2.data(), false);

    // Set rotate dir of all servo
    EXPECT_FALSE(rsi.set_rotate_dir(ids.size(), ids.data(), true));

    // Set rotate dir of first half of the servo
    EXPECT_FALSE(rsi.set_rotate_dir(sub_ids1.size(), sub_ids1.data(), true));
    EXPECT_FALSE(rsi.set_rotate_dir(sub_ids1.size(), sub_ids1.data(), false));

    // Set rotate dir of second half of the servo
    EXPECT_TRUE(rsi.set_rotate_dir(sub_ids2.size(), sub_ids2.data(), true));
    for(unsigned int k=0; k < sub_ids2.size(); k++){
        unsigned int id = sub_ids2[k];
        EXPECT_TRUE(rsi.getRotateDir(id));
    }
    EXPECT_TRUE(rsi.set_rotate_dir(sub_ids2.size(), sub_ids2.data(), false));
    for(unsigned int k=0; k < sub_ids2.size(); k++){
        unsigned int id = sub_ids2[k];
        EXPECT_FALSE(rsi.getRotateDir(id));
    }

    // Turn off all servo
    rsi.torque_onoff(ids.size(), ids.data(), false);
    // Set rotate dir of all servo to 'normal'
    EXPECT_TRUE(rsi.set_rotate_dir(ids.size(), ids.data(), false));
    for(unsigned int k=0; k < ids.size(); k++){
        unsigned int id = ids[k];
        EXPECT_FALSE(rsi.getRotateDir(id));
    }
};

TEST_F(RSInterfaceTest, DISABLED_Move0){
    double initial_angle    = 0;

    // Turn off all servo and set rotate direction to 'reverse'
    rsi.torque_onoff(ids.size(), ids.data(), false);
    EXPECT_TRUE(rsi.set_rotate_dir(ids.size(), ids.data(), true));

    // Turn on all servo
    rsi.torque_onoff(ids.size(), ids.data(), true);

    // Move all servo to initial position (angle =0, speed= 20)
    usleep(20000);      //2ms
    rsi.move(ids.size(), ids.data(), vector<double> (ids.size(), initial_angle).data(), vector<double> (ids.size(), 50).data());
    usleep(5000000);    //5sec

    // Turn off all servo and set rotate direction to 'normal'
    rsi.torque_onoff(ids.size(), ids.data(), false);
    EXPECT_TRUE(rsi.set_rotate_dir(ids.size(), ids.data(), false));
};

TEST_F(RSInterfaceTest, Move1){
    double initial_angle    = 0;
    double test_angle0      = 160;
    double test_angle1      = 3;         // For proximal joint
    double test_angle2      = 15;        // For distal joint
    double test_speed       = 100;
    double sleep_time       = 700000;
    
    // Turn off all servo and set rotate direction to 'reverse'
    rsi.torque_onoff(ids.size(), ids.data(), false);
    EXPECT_TRUE(rsi.set_rotate_dir(ids.size(), ids.data(), true));

    // Turn on all servo
    rsi.torque_onoff(ids.size(), ids.data(), true);

    // Move all servo to initial position (angle =0, speed= 20)
    usleep(20000);      //2ms
    rsi.move(ids.size(), ids.data(), vector<double> (ids.size(), initial_angle).data(), vector<double> (ids.size(), 50).data());
    usleep(5000000);    //5sec

    for(unsigned int k=0; k < ids.size(); k++){
        unsigned int id = ids[k];

        // Test move... too much angle
        EXPECT_FALSE(rsi.move(id, test_angle0, test_speed));

        // Test move... too much angle
        EXPECT_FALSE(rsi.move(id, -1.0*test_angle0, test_speed));

        double test_angle = 0;
        if(k%4 == 0){
            test_angle = test_angle1;   // Proximal joint (most inner)
        }else{
            test_angle = test_angle2;   // Distal joint (all outer)
        }

        // Test move...
        EXPECT_TRUE(rsi.move(id, test_angle, test_speed));
        usleep(sleep_time);
        EXPECT_NEAR(test_angle, rsi.getAngle(id), angle_near);
        usleep(sleep_time);

        // Test move...
        EXPECT_TRUE(rsi.move(id, -1.0*test_angle, test_speed));
        usleep(sleep_time);
        EXPECT_NEAR(-1.0*test_angle, rsi.getAngle(id), angle_near);
        usleep(sleep_time);

        // Test move...
        EXPECT_TRUE(rsi.move(id, initial_angle, test_speed));
        usleep(sleep_time);
        EXPECT_NEAR(initial_angle, rsi.getAngle(id), angle_near);
        usleep(sleep_time);
    }

    // Turn off all servo and set rotate direction to 'normal'
    rsi.torque_onoff(ids.size(), ids.data(), false);
    EXPECT_TRUE(rsi.set_rotate_dir(ids.size(), ids.data(), false));
};

TEST_F(RSInterfaceTest,DISABLED_getAngle_Loop){
    // Turn off all servo and set rotate direction to 'reverse'
    rsi.torque_onoff(ids.size(), ids.data(), false);
    EXPECT_TRUE(rsi.set_rotate_dir(ids.size(), ids.data(), true));

    // Turn on all servo
    rsi.torque_onoff(ids.size(), ids.data(), true);
    //usleep(10000);

    for(int loop = 0; loop < 10; loop++){
        cout << loop << " : ";
        for(unsigned int k = 0; k < ids.size(); k++){
            int id = ids[k];
            cout << k << "\t";
            rsi.getAngle(id);
        }
        cout << endl;
    }

    // Turn off all servo and set rotate direction to 'normal'
    rsi.torque_onoff(ids.size(), ids.data(), false);
    EXPECT_TRUE(rsi.set_rotate_dir(ids.size(), ids.data(), false));
};

