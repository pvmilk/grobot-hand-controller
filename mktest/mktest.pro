TARGET           = test_futabahand
TEMPLATE         = app
CONFIG          -= qt   #exclude the linkage to -lQtGUI and -lQtCore

INCLUDEPATH     += ../include ./

OBJECTS_DIR     += .obj

LIBS            += -L"../lib/" -lfutabahand -lpthread -lgtest -lyaml-cpp

QMAKE_CXXFLAGS  +=-g -std=c++0x

HEADERS         +=  ./RSInterfaceTest.h\
                    ./FutabaHandTest.h

SOURCES         +=  test.cpp

