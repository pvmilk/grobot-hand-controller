#include <vector>

#include "gtest/gtest.h"
#include "FutabaHand.h"

using namespace std;

class FutabaHandTest : public ::testing::Test{
    public:
        FutabaHandTest()
            :hand("../conf/joke0.yaml"){};


    virtual void SetUp(){
        angle_20 = vector<double>(angle_temp, angle_temp+20);
        angle_04 = vector<double>(angle_temp+4, angle_temp+8);
    };

    protected :
        FutabaHand hand;
        static const double angle_temp[20];
        vector<double> angle_20;
        vector<double> angle_04;
};

const double  FutabaHandTest::angle_temp[20] =    {  10, 15, 30, 45,        /// F0
                                                     -5, 15, 30, 45,        /// F1
                                                     -5, 15, 30, 45,        /// F2
                                                     -5, 15, 30, 45,        /// F3
                                                     -5, 15, 30, 45  };     /// F4

TEST_F(FutabaHandTest, Torque){
    hand.servoOff();
    usleep(20000); //20ms
    EXPECT_FALSE(hand.servoStatus());
    usleep(20000); //20ms

    hand.servoOn();
    usleep(20000); //20ms
    EXPECT_TRUE(hand.servoStatus());
    usleep(20000); //20ms

    hand.servoOff();
    usleep(20000); //20ms
    EXPECT_FALSE(hand.servoStatus());
    usleep(20000); //20ms
};

TEST_F(FutabaHandTest, SetInitial){
    hand.servoOff();
    usleep(20000); //20ms
    hand.servoOn();
    usleep(20000); //20ms

    hand.handSetInitial();
    usleep(3000000); //2sec

    hand.servoOff();
    usleep(20000); //20ms
};

TEST_F(FutabaHandTest, HandSetAngle){
    hand.servoOff();
    usleep(20000); //20ms
    hand.servoOn();
    usleep(20000); //20ms

    hand.handSetInitial();
    usleep(3000000); //3sec

    EXPECT_TRUE(hand.handSetAngle(angle_20));
    usleep(3000000); //3sec

    hand.servoOff();
    usleep(20000); //20ms
};

TEST_F(FutabaHandTest, FingerSetAngle){
    hand.servoOff();
    usleep(20000); //20ms
    hand.servoOn();
    usleep(20000); //20ms

    hand.handSetInitial();
    usleep(2000000); //2sec

    EXPECT_TRUE(hand.fingerSetAngle(0, angle_04));
    usleep(2000000); //2sec

    EXPECT_TRUE(hand.fingerSetAngle(1, angle_04));
    usleep(2000000); //2sec

    EXPECT_TRUE(hand.fingerSetAngle(2, angle_04));
    usleep(2000000); //2sec

    EXPECT_TRUE(hand.fingerSetAngle(3, angle_04));
    usleep(2000000); //2sec

    EXPECT_TRUE(hand.fingerSetAngle(4, angle_04));
    usleep(2000000); //2sec

    hand.servoOff();
    usleep(20000); //20ms

};

TEST_F(FutabaHandTest, JointSetAngle){
    hand.servoOff();
    usleep(20000); //20ms
    hand.servoOn();
    usleep(20000); //20ms

    hand.handSetInitial();
    usleep(2000000); //2sec

    for(unsigned int dof = 0; dof < hand.getHandDOFs(); dof++){
        int f = dof/4;
        int j = dof - f*4;
        EXPECT_TRUE(hand.jointSetAngle(f, j, angle_20[dof]));
        usleep(1000000); //1sec
    }

    hand.servoOff();
    usleep(20000); //20ms
};

