/**
 * \file hand_server.cpp
 * \class HandServer
 * 
 * \brief Implementation of Class HandServer
 *
 * \author   Milk Phongtharin; (pv-milk@cvl.iis.u-tokyo.ac.jp)
 */

#include <iostream>
#include <pthread.h>
#include <vector>

#include "FutabaHand.h"
#include "socket_listener.h"
#include "threadpool.h"

#include "hand_server.h"

using namespace std;

HandServer::HandServer(const char* hand_conf, const char* serv_port, unsigned int num_of_workers)
    :isDone(false){
    hand = new FutabaHand(hand_conf);
    pthread_mutex_init(&lock_mu_hand, NULL);

    angles = new double[hand->getHandDOFs()];
    pthread_rwlock_init(&lock_rw_angles, NULL);

    voltages = new double[hand->getHandDOFs()];
    pthread_rwlock_init(&lock_rw_voltages, NULL);

    if(num_of_workers == 0)
        thrdpool_workers = new ThreadPool(1);               // MUST HAVE AT LEAST 1 WORKER
    else
        thrdpool_workers = new ThreadPool(num_of_workers);
    thrdpool_workers->initializeThreads();

    thrd_sock_list = new SocketListener(serv_port, this);
};

HandServer::HandServer(const HandServer& hs)
    :isDone(hs.isDone){
};

HandServer::~HandServer(){
    pthread_rwlock_destroy(&lock_rw_angles);
    pthread_rwlock_destroy(&lock_rw_voltages);
    pthread_mutex_destroy(&lock_mu_hand);

    pthread_cancel(thrd_hand_conditions_read);
    pthread_join(thrd_hand_conditions_read, NULL);

    /*
     * destroyPool(int maxPollSecs)
     *
     * If the work is still not done, then it will check again after maxPollSecs.
     * */
    thrdpool_workers->destroyPool(5);

    delete thrd_sock_list;
    delete hand;
    delete angles;
    delete voltages;
};

HandServer& HandServer::operator=(const HandServer& hs){
    isDone = hs.isDone;
    return *this;
};

void HandServer::start(){
    // START angles_read thread;
    int iret_angles_r = pthread_create(&thrd_hand_conditions_read, NULL, update_conditions_loop, getSelfReference());

    thrd_sock_list->start();

    if(iret_angles_r != 0){
        std::cerr << "Cannot create thread to read condition... exit" << std::endl;
        isDone = true;
    }

    while(!isDone){
        usleep(100000);
    }
};

HandServer* HandServer::getSelfReference(){
    return this;
};

/// STATIC Method
void* HandServer::update_conditions_loop(void* arg){
    HandServer* object_ref = (HandServer*) arg;

    while(!object_ref->isDone){
        usleep(10000);                              // MUST : Otherwise: other thread will not be able to get the mutex at all

        vector<Condition> conditions;               // Might be a bit faster, if this is a static variable (No need to create every loop).
        // LOCK Hand mutex and update DOF value
        pthread_mutex_lock(&object_ref->lock_mu_hand);
        object_ref->hand->handGetCondition(conditions);
        pthread_mutex_unlock(&object_ref->lock_mu_hand);

        /*
         *  Cannot do 'copy' as the data structure is vertical.
         *
         *  If other attributes, e.g. time, speed, temp, ..., are necessary, 
         *  create their own 'array' and 'mutex' and copy them. 
         *
         *  However, may be not a good idea to lock mutex of 2 attributes at the same time.
         *  Do it serially. (AVOID DEADLOCK?)
         * */
        pthread_rwlock_wrlock(&object_ref->lock_rw_angles);
        for(int idx = 0; idx < object_ref->hand->getHandDOFs(); idx++){             // No mutex required for hand->getHandDOFs()
            object_ref->angles[idx] = conditions[idx].getAngle();                   // Copy angle
        }
        pthread_rwlock_unlock(&object_ref->lock_rw_angles);

        pthread_rwlock_wrlock(&object_ref->lock_rw_voltages);
        for(int idx = 0; idx < object_ref->hand->getHandDOFs(); idx++){             // No mutex required for hand->getHandDOFs()
            object_ref->voltages[idx] = conditions[idx].getVoltage();               // Copy voltage
        }
        pthread_rwlock_unlock(&object_ref->lock_rw_voltages);
    }

    return NULL;
};

/////////////////////////// CALLBACK INTERFACES ///////////////////////////////////////
void HandServer::cb_servoOn(){
    pthread_mutex_lock(&lock_mu_hand);
    hand->servoOn();
    pthread_mutex_unlock(&lock_mu_hand);
};

void HandServer::cb_servoOff(){
    pthread_mutex_lock(&lock_mu_hand);
    hand->servoOff();
    pthread_mutex_unlock(&lock_mu_hand);
};

bool HandServer::cb_servoStatus(){
    pthread_mutex_lock(&lock_mu_hand);
    bool status = hand->servoStatus();
    pthread_mutex_unlock(&lock_mu_hand);

    return status;
};

void HandServer::cb_handSetInitial(){
    pthread_mutex_lock(&lock_mu_hand);
    hand->handSetInitial();
    pthread_mutex_unlock(&lock_mu_hand);
};

bool HandServer::cb_handSetAngle(vector<double> &dofs){
    pthread_mutex_lock(&lock_mu_hand);
    bool isSuccess = hand->handSetAngle(dofs);
    pthread_mutex_unlock(&lock_mu_hand);

    return isSuccess;
};

void HandServer::cb_handGetAngle(vector<double> &dofs){
    int dof_num = hand->getHandDOFs();              // No mutex required for hand->getHandDOFs()

    pthread_rwlock_rdlock(&lock_rw_angles);

    dofs.assign(angles, angles+dof_num);            // Size would be assigned accordingly
    pthread_rwlock_unlock(&lock_rw_angles);
};

void HandServer::cb_handGetVoltage(vector<double> &vtgs){
    int dof_num = hand->getHandDOFs();              // No mutex required for hand->getHandDOFs()

    pthread_rwlock_rdlock(&lock_rw_voltages);

    vtgs.assign(voltages, voltages+dof_num);            // Size would be assigned accordingly
    pthread_rwlock_unlock(&lock_rw_voltages);
};

bool HandServer::cb_fingerSetAngle(unsigned int f_idx,  vector<double> &dofs){
    pthread_mutex_lock(&lock_mu_hand);
    bool isSuccess = hand->fingerSetAngle(f_idx, dofs);
    pthread_mutex_unlock(&lock_mu_hand);

    return isSuccess;
};

void HandServer::cb_fingerGetAngle(unsigned int f_idx, vector<double> &dofs){
    unsigned int begin_dof = 0;
    unsigned int end_dof   = 0;
    for(unsigned int i = 0; i < hand->getFingerNum(); i++){
        int c_ndof = hand->getFingerDOFs(i); // No mutex required for hand->getHandDOFs()
        if(i == f_idx){
            end_dof   += c_ndof;
            break;
        }else{
            begin_dof += c_ndof;
            end_dof   += c_ndof;
        }
    }

    pthread_rwlock_rdlock(&lock_rw_angles);

    dofs.assign(angles + begin_dof, angles + end_dof);            // Size would be assigned accordingly
    pthread_rwlock_unlock(&lock_rw_angles);
};

bool HandServer::cb_jointSetAngle(unsigned int f_idx, unsigned int j_idx, double angle){
    pthread_mutex_lock(&lock_mu_hand);
    bool isSuccess = hand->jointSetAngle(f_idx, j_idx, angle);
    pthread_mutex_unlock(&lock_mu_hand);

    return isSuccess;

};

double HandServer::cb_jointGetAngle(unsigned int f_idx, unsigned int j_idx){
    int idx = 0;
    for(unsigned int i = 0; i < hand->getFingerNum(); i++){
        if(i >= f_idx){
            break;
        }else{
            idx += hand->getFingerDOFs(i); // No mutex required for hand->getHandDOFs()
        }
    }
    idx += j_idx;

    pthread_rwlock_rdlock(&lock_rw_angles);

    double angle = angles[idx];
    pthread_rwlock_unlock(&lock_rw_angles);

    return angle;
};

unsigned int HandServer::cb_getHandDOFs(){
    return hand->getHandDOFs();              // No mutex required for hand->getHandDOFs()
};

unsigned int HandServer::cb_getFingerNum(){
    return hand->getFingerNum();             // No mutex required for hand->getHandDOFs()
};

unsigned int HandServer::cb_getFingerDOFs(unsigned int f_idx){
    return hand->getFingerDOFs(f_idx);    // No mutex required for hand->getHandDOFs()
};

//EOF
