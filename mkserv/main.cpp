#include <unistd.h>
#include <lo/lo.h>
#include <pthread.h>
#include <iostream>
#include <sstream>
#include <string>
#include <getopt.h>

#include "hand_server.h"
#include "FutabaHand.h"

using namespace std;

/* Flag set by $B!F(B--help$B!G(B. */
static bool help_flag = false;

void print_help(){
        cout << endl;
        cout << "Usage: ./hand-osc-server -c handcfg.yaml [OPTIONS]" << endl;
        cout << endl;
        cout << "   -h     --help                             Print this help" << endl;
        cout << "   -c     --config $CONF/handcfg.yaml        Indicate hand configuration file (Required)." << endl;
        cout << "   -p     --port 9898                        Indicate listening port for OSC server (Optional)." << endl;
        cout << "   -w     --workers 10                       Indicate number of workers to execute command (Optional)." << endl;
}

int main (int argc, char **argv){

    int     opt_error = 0;
    bool    isHandCfg = false;
    string  hand_conf;
    string  port("9898");
    unsigned int num_workers  = 10;

    while(true){
        static struct option long_options[] ={
            /* These options don't set a flag. We distinguish them by their indices. */
            {"help",        no_argument,                0, 'h'},
            {"config",      required_argument,          0, 'c'},
            {"port",        required_argument,          0, 'p'},
            {"workers",     required_argument,          0, 'w'},
            {0,             0,                          0,  0}
        };

        /* getopt_long stores the option index here. */
        int option_index = 0;

        int command = getopt_long (argc, argv, "hc:p:w:",long_options, &option_index);

        /* Detect the end of the options. */
        if (command == -1)
            break;

        switch(command){
            case 0:  {
                         break;
                     }
            case 'h':{
                         help_flag = true;
                         break;
                     }
            case 'c':{
                         isHandCfg = true;
                         hand_conf = string(optarg);
                         break;
                     }
            case 'p':{
                         istringstream port_stream(optarg);
                         unsigned int parse = 0;
                         if(!(port_stream >> parse).fail()){
                            port = string(optarg);
                         }else{
                            cout << "INVALID Port; Fall to default port(9898)" << endl;
                         }
                         break;
                     }
            case 'w':{
                         istringstream worker_stream(optarg);
                         unsigned int parse = 0;
                         if(!(worker_stream >> parse).fail()){
                             num_workers = parse;
                         }else{
                            cout << "INVALID Number of workers; Fall to default (10)" << endl;
                         }
                         break;
                     }
            case '?':{
                         // getopt_long already printed an error message
                         opt_error = -1;
                         break;
                     }
            default:
                     abort();
        }
    }

    if(help_flag){
        opt_error = -2;
    }else if(!isHandCfg){
        cout << "./hand-osc-server: please also specify hand configuration file" << endl;
        opt_error = -3;
    }

    if(opt_error < 0){
        print_help();
    }else{
        cout << "Start server with the following conditions : " << endl;
        cout << "\thand_configuration_file : " << hand_conf << endl;
        cout << "\tOSC listening port      : " << port << endl;
        cout << "\tNumber of Workers       : " << num_workers << endl;
        HandServer  hand_server(hand_conf.c_str(), port.c_str(), num_workers);

        //blocking
        hand_server.start();
    }



    return 0;
}
