/*
 *  Copyright (C) 2012 Steve Harris, Stephen Sinclair
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 2.1 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  $Id$
 */

/* Run this program in one terminal with no arguments,
 *   $ ./example_tcp_echo_server
 * and in another terminal with argument 1,
 *   $ ./example_tcp_echo_server 1
 * Now watch the /test message ping back and forth a few times!
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <vector>

#include "lo/lo.h"

#define PROTOCOL LO_UDP
//#define PROTOCOL LO_TCP

using namespace std;
int done = 0;

void error(int num, const char *m, const char *path);

int handGetBlobDouble_handler(const char *path, const char *types, lo_arg ** argv,
                    int argc, void *data, void *user_data);

int fingerGetAngle_handler(const char *path, const char *types, lo_arg ** argv,
                    int argc, void *data, void *user_data);


int echo_handler(const char *path, const char *types, lo_arg ** argv,
                 int argc, void *data, void *user_data);

int quit_handler(const char *path, const char *types, lo_arg ** argv,
                 int argc, void *data, void *user_data);

void ctrlc(int sig)
{
    done = 1;
}

int main(int argc, char *argv[])
{
    const char *port = "7770";
    int do_send = 0;

    /* start a new server on port 7770 */
    lo_server_thread st = lo_server_thread_new_with_proto(port, PROTOCOL, error);
    if (!st) {
        printf("Could not create server thread.\n");
        exit(1);
    }

    lo_server s = lo_server_thread_get_server(st);

    lo_server_thread_add_method(st, "/quit", "", quit_handler, NULL);

    lo_server_thread_add_method(st, "/ans/read/handGetAngle", NULL, handGetBlobDouble_handler, s);

    lo_server_thread_add_method(st, "/ans/read/handGetVoltage", NULL, handGetBlobDouble_handler, s);

    lo_server_thread_add_method(st, "/ans/read/fingerGetAngle", NULL, fingerGetAngle_handler, s);

    lo_server_thread_add_method(st, NULL, NULL, echo_handler, s);

    lo_server_thread_start(st);

    signal(SIGINT,ctrlc);

    printf("Listening on port %d\n", lo_server_thread_get_port(st));

    lo_address a = lo_address_new_with_proto(PROTOCOL, "192.168.100.22", "9898");

    int r = 0;

    int usleep_time = 100000;
    //int commands_arr[] = {0, 3};
    //vector<int> commands(commands_arr, commands_arr + sizeof(commands_arr)/sizeof(int));
    vector<int> commands(100000, 6);

    for(int c_idx = 0; c_idx < commands.size(); c_idx++){
        if (!a) {
            printf("Error creating destination address.\n");
            exit(1);
        }

        int msg_tag = c_idx;
        int command = commands[c_idx];
        printf("Sending with msg_tag : %d, through port %d\n", msg_tag, lo_server_get_port(s));

        switch(command){
            case 0:{    r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/write/servoOn", "i", msg_tag);
                        break;}
            case 1:{    r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/write/servoOff", "i", msg_tag);
                        break;}
            case 2:{    r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/read/servoStatus", "i", msg_tag);
                        break;}
            case 3:{    r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/write/handSetInitial", "i", msg_tag);
                        break;}
            case 4:{    vector<double> dofs;
                        dofs.assign(20, 15);
                        lo_blob dofs_b = lo_blob_new(sizeof(double)*dofs.size(), dofs.data());
                        r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/write/handSetAngle", "ibi", msg_tag, dofs_b, sizeof(double));
                        lo_blob_free(dofs_b);
                        break;}
            case 5:{    r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/read/handGetAngle", "i", msg_tag);
                        break;}
            case 6:{    r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/read/handGetVoltage", "i", msg_tag);
                        break;}
            case 7:{    vector<double> f_dofs;
                        f_dofs.assign(4, -15);
                        lo_blob f_dofs_b = lo_blob_new(sizeof(double)*f_dofs.size(), f_dofs.data());
                        r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/write/fingerSetAngle", "iibi", msg_tag, 0, f_dofs_b, sizeof(double));
                        lo_blob_free(f_dofs_b);
                        break;}
            case 8:{    r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/read/fingerGetAngle", "ii", msg_tag, 5);
                        break;}
            case 9:{    r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/write/jointSetAngle", "iiid", msg_tag, 0, 0, 50.0);
                        break;}
            case 10:{    r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/read/jointGetAngle", "iii", msg_tag, 0, 0);
                        break;}
            case 11:{   r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/read/getHandDOFs", "i", msg_tag);
                        break;}
            case 12:{   r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/read/getFingerNum", "i", msg_tag);
                        break;}
            case 13:{   r = lo_send_from(a, s, LO_TT_IMMEDIATE, "/read/getFingerDOFs", "ii", msg_tag, 3);
                        break;}
        }

        if (r < 0)
            printf("Error sending initial message.\n");

        usleep(usleep_time);
    }

    // Wait for final reply
    usleep(5000000);

    if (a)
        lo_address_free(a);

    lo_server_thread_free(st);

    return 0;
}

void error(int num, const char *msg, const char *path)
{
    printf("liblo server error %d in path %s: %s\n", num, path, msg);
    fflush(stdout);
}

int handGetBlobDouble_handler(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data){
    int i;

    printf("path: <%s>\n", path);
    printf("msg_tag: <%d>\n", argv[0]->i);
    lo_blob dofs_b = argv[1];

    int num_ele = lo_blob_datasize(dofs_b)/sizeof(double);
    double* dat = (double*) lo_blob_dataptr(dofs_b);
    vector<double> dofs(dat, dat + num_ele);
    for(int i = 0; i < dofs.size(); i++){
        printf("%f, ", dofs[i]);
    }
    printf("\n");

    return 0;
}

int fingerGetAngle_handler(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data){
    int i;

    printf("path: <%s>\n", path);
    printf("msg_tag: <%d>\n", argv[0]->i);
    
    lo_blob dofs_b = argv[1];

    int num_ele = lo_blob_datasize(dofs_b)/sizeof(double);
    double* dat = (double*) lo_blob_dataptr(dofs_b);
    vector<double> dofs(dat, dat + num_ele);
    for(int i = 0; i < dofs.size(); i++){
        printf("%f, ", dofs[i]);
    }
    printf("\n");

    return 0;
}


int echo_handler(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data){
    int i;

    printf("path: <%s>\n", path);
    printf("msg_tag: <%d>\n", argv[0]->i);

    for (i = 1; i < argc; i++) {
        printf("arg %d '%c' ", i, types[i]);
        lo_arg_pp((lo_type)types[i], argv[i]);
        printf("\n");
    }

    //lo_message m = (lo_message)data;
    //lo_address a = lo_message_get_source(m);
    //lo_server s = (lo_server)user_data;
    //const char *host = lo_address_get_hostname(a);
    //const char *port = lo_address_get_port(a);
    //printf("host:port = %s:%s\n", host, port);

    return 0;
}


int quit_handler(const char *path, const char *types, lo_arg ** argv,
                 int argc, void *data, void *user_data)
{
    done = 1;
    printf("quitting\n\n");
    fflush(stdout);

    return 0;
}
