#!/usr/bin/env python

import liblo, sys, time, random

protocol = liblo.UDP
#protocol = liblo.TCP

class MyServer(liblo.ServerThread):
    def __init__(self, port):
        liblo.ServerThread.__init__(self, port, protocol)

    def ans_callback(self, path, args):
        print "received message '%s' with arguments: %s" % (path, args)

    def fallback(self, path, args):
        print "received unknown message '%s'" % path

try:
    server = MyServer(2222)
except liblo.ServerError, err:
    print str(err)
    sys.exit()

server.add_method("/ans/read", None, server.ans_callback)
server.add_method(None, None, server.fallback)

print "Start listening server at port :", server.get_port();
server.start()

target = None
try:
    target = liblo.Address("192.168.100.22", 9898, protocol)
except liblo.AddressError, err:
    print str(err)
    sys.exit()

angle = random.random()*110

# send message "/foo/message1" with int, float and string arguments
for i in range(0, 1):
    print i
    # If server exited, it will automatically use the same socket port
    server.send(target, "/read")
    #liblo.send(target, "/read", 124, 456.789, "test")
    time.sleep(1.0/50);
    #time.sleep(10);
#    #angle = (angle+30)%110
#    #liblo.send(target, "/write", -1.0*angle)
#    #time.sleep(4.0);

liblo.send(target, "/quit")

