/**
 * \file hand_server.h
 * \class Example
 * 
 * \brief Header of Class HandServer
 *
 * \author Milk Phongtharin; (pv-milk@cvl.iis.u-tokyo.ac.jp)
 */

#ifndef _HandServer_H_
#define _HandServer_H_

#include <lo/lo.h>
#include <pthread.h>
#include <vector>

#include "threadpool.h"

class  FutabaHand;
class  SocketListener;

/*!
 *  Class HandServer
 *
 */
class HandServer{
    friend class SocketListener;

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        HandServer(const char* hand_conf = "../conf/handcfg.yaml", const char* serv_port = "9898", unsigned int num_of_workers = 10);

        /// Copy Constructor
        HandServer(const HandServer& hs);

        /// Destructor
        ~HandServer();

        /// Operator=
        HandServer& operator=(const HandServer& hs);

        /// Blocking the thread
        void start();

        /// Method for thrd_hand_conditions_read
        static void* update_conditions_loop(void* arg);

    // All Interfaces for SocketListener to get infos or execute hand command.
    public:
        virtual void cb_servoOn();

        virtual void cb_servoOff();

        virtual bool cb_servoStatus();

        virtual void cb_handSetInitial();

        virtual bool cb_handSetAngle(std::vector<double> &dofs);

        virtual void cb_handGetAngle(std::vector<double> &dofs);

        virtual void cb_handGetVoltage(std::vector<double> &vtgs);

        virtual bool cb_fingerSetAngle(unsigned int f_idx, std::vector<double> &dofs);

        virtual void cb_fingerGetAngle(unsigned int f_idx, std::vector<double> &dofs);

        virtual bool cb_jointSetAngle(unsigned int f_idx, unsigned int j_idx, double angle);

        virtual double cb_jointGetAngle(unsigned int f_idx, unsigned int j_idx);

        virtual unsigned int cb_getHandDOFs();

        virtual unsigned int cb_getFingerNum();

        virtual unsigned int cb_getFingerDOFs(unsigned int f_idx);

    private:
        HandServer* getSelfReference();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
        FutabaHand*         hand;
        pthread_mutex_t     lock_mu_hand;               // MUTEX Locks for hand

        double*             angles;
        pthread_rwlock_t    lock_rw_angles;             // Read-Write Locks for angles

        double*             voltages;
        pthread_rwlock_t    lock_rw_voltages;           // Read-Write Locks for voltages

        ThreadPool*         thrdpool_workers;

        SocketListener*     thrd_sock_list;             // OSC Thread to listen to the socket
        pthread_t           thrd_hand_conditions_read;  // Pthread that read hand and update the cache value

        bool                isDone;
};

#endif//_HandServer_H_
//EOF
