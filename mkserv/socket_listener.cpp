/**
 * \file socket_listener.cpp
 * \class SocketListener
 * 
 * \brief Implementation of Class SocketListener
 *
 * \author VINAYAVEKHIN Phongtharin
 *
 *
 * NOTE: Reference on pointer-to-member-function; Reason why the callback handler is a 'static' function
 *      - http://www.parashift.com/c++-faq-lite/memfnptr-vs-fnptr-more.html
 *      - http://cboard.cprogramming.com/cplusplus-programming/97397-passing-method-pointer-function-pointer.html
 *
 */

#ifdef UNUSED
#elif defined(__GNUC__) 
# define UNUSED(x) UNUSED_ ## x __attribute__((unused)) 
#elif defined(__LCLINT__) 
# define UNUSED(x) /*@unused@*/ x 
#else 
# define UNUSED(x) x 
#endif


#include <pthread.h>
#include <iostream>
#include <lo/lo.h>
#include "hand_server.h"
#include "workers/wtw_servoOn.h"
#include "workers/wtw_servoOff.h"
#include "workers/wtr_servoStatus.h"
#include "workers/wtw_handSetInitial.h"
#include "workers/wtw_handSetAngle.h"
#include "workers/wtr_handGetAngle.h"
#include "workers/wtr_handGetVoltage.h"
#include "workers/wtw_fingerSetAngle.h"
#include "workers/wtr_fingerGetAngle.h"
#include "workers/wtw_jointSetAngle.h"
#include "workers/wtr_jointGetAngle.h"
#include "workers/wtr_getHandDOFs.h"
#include "workers/wtr_getFingerNum.h"
#include "workers/wtr_getFingerDOFs.h"

#include "socket_listener.h"

using namespace std;

unsigned int SocketListener::latest_wid = 0;

SocketListener::SocketListener(const char* port, HandServer* cb)
    :handsrv_cb(cb){

    pthread_mutex_init(&lock_mu_write_sock, NULL);

    // start a new server on UDP:port
    thrd_server = lo_server_thread_new_with_proto(port, LO_UDP, SocketListener::handler_sock_error);

    /*
     * path : /write/servoOn
     * arg  : int[0]  :  msg tag
     * */
    lo_server_thread_add_method(thrd_server, "/write/servoOn", "i", SocketListener::handler_write_servoon, getSelfReference());

    /*
     * path : /write/servoOff
     * arg  : int[0]  :  msg tag
     * */
    lo_server_thread_add_method(thrd_server, "/write/servoOff", "i", SocketListener::handler_write_servooff, getSelfReference());

    /*
     * path : /read/servoStatus
     * arg  : int[0]  :  msg tag
     * */
    lo_server_thread_add_method(thrd_server, "/read/servoStatus", "i", SocketListener::handler_read_servostatus, getSelfReference());

    /*
     * path : /write/handSetInitial
     * arg  : int[0]  :  msg tag
     * */
    lo_server_thread_add_method(thrd_server, "/write/handSetInitial", "i", SocketListener::handler_write_handsetinitial, getSelfReference());

    /*
     * path : /write/handSetAngle
     * arg  : int[0]  :  msg tag
     *        blob[1] :  array of bytes (desired of the corresponded fingers; size MUST be matched)
     *        int[2]  :  size in bytes of each element of the array
     * */
    lo_server_thread_add_method(thrd_server, "/write/handSetAngle", "ibi", SocketListener::handler_write_handsetangle, getSelfReference());

    /*
     *  path : /read/handGetAngle
     *  arg  : int[0] :  msg tag
     * */
    lo_server_thread_add_method(thrd_server, "/read/handGetAngle", "i", SocketListener::handler_read_handgetangle, getSelfReference());

    /*
     *  path : /read/handGetVoltage
     *  arg  : int[0] :  msg tag
     * */
    lo_server_thread_add_method(thrd_server, "/read/handGetVoltage", "i", SocketListener::handler_read_handgetvoltage, getSelfReference());

    /*
     *  path : /write/fingerSetAngle
     *  arg  : int[0]    :  msg tag
     *         int[1]    :  finger index, [0, fingerNum)
     *         blob[2]   :  array of bytes (desired of the corresponded fingers; size MUST be matched)
     *         int[3]    :  size in bytes of each element of the array
     *
     * */
    lo_server_thread_add_method(thrd_server, "/write/fingerSetAngle", "iibi", SocketListener::handler_write_fingersetangle, getSelfReference());

    /*
     *  path : /read/fingerGetAngle
     *  arg  : int[0] :  msg tag
     *         int[1] :  finger index, [0, fingerNum)
     * */
    lo_server_thread_add_method(thrd_server, "/read/fingerGetAngle", "ii", SocketListener::handler_read_fingergetangle, getSelfReference());

    /*
     *  path : /write/jointSetAngle
     *  arg  : int[0]    :  msg tag
     *         int[1]    :  finger index, [0, fingerNum)
     *         int[2]    :  joint index,  [0, jointNum(finger_idx))
     *         double[3] :  desired angle
     * */
    lo_server_thread_add_method(thrd_server, "/write/jointSetAngle", "iiid", SocketListener::handler_write_jointsetangle, getSelfReference());

    /*
     *  path : /read/jointGetAngle
     *  arg  : int[0] :  msg tag
     *         int[1] :  finger index, [0, fingerNum)
     *         int[2] :  joint index,  [0, jointNum(finger_idx))
     * */
    lo_server_thread_add_method(thrd_server, "/read/jointGetAngle", "iii", SocketListener::handler_read_jointgetangle, getSelfReference());

    /*
     *  path : /read/getHandDOFs
     *  arg  : int[0] :  msg tag
     * */
    lo_server_thread_add_method(thrd_server, "/read/getHandDOFs", "i", SocketListener::handler_read_gethanddofs, getSelfReference());

    /*
     *  path : /read/getFingerNum
     *  arg  : int[0] :  msg tag
     * */
    lo_server_thread_add_method(thrd_server, "/read/getFingerNum", "i", SocketListener::handler_read_getfingernum, getSelfReference());

    /*
     *  path : /read/getFingerDOFs
     *  arg  : int[0] :  msg tag
     *         int[1] :  finger index, [0, fingerNum)
     * */
    lo_server_thread_add_method(thrd_server, "/read/getFingerDOFs", "ii", SocketListener::handler_read_getfingerdofs, getSelfReference());

    // add method that will match the path /quit with no args
    lo_server_thread_add_method(thrd_server, "/quit", "", SocketListener::handler_quit, getSelfReference());
};

SocketListener::SocketListener(const SocketListener& UNUSED(sl)){
    // TODO
};

SocketListener::~SocketListener(){
    /* clear thread*/
    pthread_mutex_destroy(&lock_mu_write_sock);

    lo_server_thread_free(thrd_server);
};

SocketListener& SocketListener::operator=(const SocketListener& UNUSED(sl)){
    // TODO
    return *this;
};

void SocketListener::start(){
    // Non-blocking method
    lo_server_thread_start(thrd_server);
}

SocketListener* SocketListener::getSelfReference(){
    return this;
};

void SocketListener::lockSockWriteMutex(){
    pthread_mutex_lock(&lock_mu_write_sock);
};

void SocketListener::releaseSockWriteMutex(){
    pthread_mutex_unlock(&lock_mu_write_sock);
};

//Static Method
void SocketListener::handler_sock_error(int UNUSED(num), const char *UNUSED(msg), const char *UNUSED(path)){

};

struct read_dat{
    SocketListener* object_ref;
    void*           lo_data;
};

//Static Method
int SocketListener::handler_write_servoon(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 1) & ((lo_type) types[0] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    // msg tag
    int m_tag = argv[0]->i;

    SocketListener* object_ref = (SocketListener*) user_data;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WThreadOSC_ARG args(m_tag, path);

    WTWriteServoOn* work = new WTWriteServoOn(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_write_servooff(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 1) & ((lo_type) types[0] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    // msg tag
    int m_tag = argv[0]->i;

    SocketListener* object_ref = (SocketListener*) user_data;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WThreadOSC_ARG args(m_tag, path);

    WTWriteServoOff* work = new WTWriteServoOff(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_read_servostatus(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 1) & ((lo_type) types[0] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    // msg tag
    int m_tag = argv[0]->i;

    SocketListener* object_ref = (SocketListener*) user_data;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WThreadOSC_ARG args(m_tag, path);

    WTReadServoStatus* work = new WTReadServoStatus(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_write_handsetinitial(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 1) & ((lo_type) types[0] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    // msg tag
    int m_tag = argv[0]->i;

    SocketListener* object_ref = (SocketListener*) user_data;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WThreadOSC_ARG args(m_tag, path);

    WTWriteHandSetInitial* work = new WTWriteHandSetInitial(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_write_handsetangle(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 3) & ((lo_type) types[0] != LO_INT32) & ((lo_type) types[1] != LO_BLOB) & ((lo_type) types[2] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    // msg tag
    int m_tag = argv[0]->i;

    SocketListener* object_ref = (SocketListener*) user_data;

    // Parse dofs value from blob
    lo_blob dofs_b = argv[1];
    int num_ele    = lo_blob_datasize(dofs_b)/argv[2]->i;

    if(num_ele <= 0 || num_ele > (int) object_ref->handsrv_cb->cb_getHandDOFs()){
        return 1;
    }
    double* dofs_dat = (double*) lo_blob_dataptr(dofs_b);
    vector<double> dofs(dofs_dat, dofs_dat+num_ele);

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WTThreadOSCD_ARG args(m_tag, path, dofs);

    WTWriteHandSetAngle* work = new WTWriteHandSetAngle(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_read_handgetangle(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 1) & ((lo_type) types[0] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    // msg tag
    int m_tag = argv[0]->i;

    SocketListener* object_ref = (SocketListener*) user_data;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WThreadOSC_ARG args(m_tag, path);

    WTReadHandGetAngle* work = new WTReadHandGetAngle(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_read_handgetvoltage(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 1) & ((lo_type) types[0] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    // msg tag
    int m_tag = argv[0]->i;

    SocketListener* object_ref = (SocketListener*) user_data;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WThreadOSC_ARG args(m_tag, path);

    WTReadHandGetVoltage* work = new WTReadHandGetVoltage(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_write_fingersetangle(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 4) & ((lo_type) types[0] != LO_INT32) & ((lo_type) types[1] != LO_INT32) & ((lo_type) types[2] != LO_BLOB) & ((lo_type) types[3] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    SocketListener* object_ref = (SocketListener*) user_data;

    // finger index
    int finger_idx = argv[1]->i;
    if(finger_idx < 0 || finger_idx >= (int) object_ref->handsrv_cb->cb_getFingerNum()){
        return 1;
    }

    // Parse dofs value from blob
    lo_blob dofs_b = argv[2];
    int num_ele    = lo_blob_datasize(dofs_b)/argv[3]->i;

    if(num_ele <= 0 || num_ele > (int) object_ref->handsrv_cb->cb_getFingerDOFs(finger_idx)){
        return 1;
    }
    double* dofs_dat = (double*) lo_blob_dataptr(dofs_b);
    vector<double> dofs(dofs_dat, dofs_dat+num_ele);

    // msg tag
    int m_tag = argv[0]->i;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WTFingerD_ARG args(m_tag, path, finger_idx, dofs);

    WTWriteFingerSetAngle* work = new WTWriteFingerSetAngle(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_read_fingergetangle(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 2) & ((lo_type) types[0] != LO_INT32) & ((lo_type) types[1] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    SocketListener* object_ref = (SocketListener*) user_data;

    // finger index
    int finger_idx = argv[1]->i;
    if(finger_idx < 0 || finger_idx >= (int) object_ref->handsrv_cb->cb_getFingerNum()){
        return 1;
    }

    // msg tag
    int m_tag = argv[0]->i;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WTFinger_ARG args(m_tag, path, finger_idx);

    WTReadFingerGetAngle* work = new WTReadFingerGetAngle(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

int SocketListener::handler_write_jointsetangle(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 4) & ((lo_type) types[0] != LO_INT32) & ((lo_type) types[1] != LO_INT32) & ((lo_type) types[2] != LO_INT32) & ((lo_type) types[3] != LO_DOUBLE)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    SocketListener* object_ref = (SocketListener*) user_data;

    // finger index
    int finger_idx = argv[1]->i;
    if(finger_idx < 0 || finger_idx >= (int) object_ref->handsrv_cb->cb_getFingerNum()){
        return 1;
    }

    // joint index
    int joint_idx = argv[2]->i;
    if(joint_idx < 0 || joint_idx >= (int) object_ref->handsrv_cb->cb_getFingerDOFs(finger_idx)){
        return 1;
    }

    double angle = argv[3]->d;

    // msg tag
    int m_tag = argv[0]->i;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WTJointD_ARG args(m_tag, path, finger_idx, joint_idx, angle);

    WTWriteJointSetAngle* work = new WTWriteJointSetAngle(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

int SocketListener::handler_read_jointgetangle(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 3) & ((lo_type) types[0] != LO_INT32) & ((lo_type) types[1] != LO_INT32) & ((lo_type) types[2] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    SocketListener* object_ref = (SocketListener*) user_data;

    // finger index
    int finger_idx = argv[1]->i;
    if(finger_idx < 0 || finger_idx >= (int) object_ref->handsrv_cb->cb_getFingerNum()){
        return 1;
    }

    // joint index
    int joint_idx = argv[2]->i;
    if(joint_idx < 0 || joint_idx >= (int) object_ref->handsrv_cb->cb_getFingerDOFs(finger_idx)){
        return 1;
    }

    // msg tag
    int m_tag = argv[0]->i;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WTJoint_ARG args(m_tag, path, finger_idx, joint_idx);

    WTReadJointGetAngle* work = new WTReadJointGetAngle(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_read_gethanddofs(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 1) & ((lo_type) types[0] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    // msg tag
    int m_tag = argv[0]->i;

    SocketListener* object_ref = (SocketListener*) user_data;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WThreadOSC_ARG args(m_tag, path);

    WTReadGetHandDOFs* work = new WTReadGetHandDOFs(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};


//Static Method
int SocketListener::handler_read_getfingernum(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 1) & ((lo_type) types[0] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    // msg tag
    int m_tag = argv[0]->i;

    SocketListener* object_ref = (SocketListener*) user_data;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WThreadOSC_ARG args(m_tag, path);

    WTReadGetFingerNum* work = new WTReadGetFingerNum(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_read_getfingerdofs(const char *path, const char *types, lo_arg ** argv, int argc, void* data, void* user_data){
    /*
     * Check consistency of the input
     * */
    if((argc != 2) & ((lo_type) types[0] != LO_INT32) & ((lo_type) types[1] != LO_INT32)){
        return 1;       /// Let's the default handler to handler it. (send back error)
    }

    SocketListener* object_ref = (SocketListener*) user_data;

    // finger index
    int finger_idx = argv[1]->i;
    if(finger_idx < 0 || finger_idx >= (int) object_ref->handsrv_cb->cb_getFingerNum()){
        return 1;
    }

    // msg tag
    int m_tag = argv[0]->i;

    lo_message msg  = (lo_message) data;
    lo_address target_addr = lo_message_get_source(msg);

    WTFinger_ARG args(m_tag, path, finger_idx);

    WTReadGetFingerDOFs* work = new WTReadGetFingerDOFs(latest_wid++, object_ref, target_addr, args);

    object_ref->handsrv_cb->thrdpool_workers->assignWork(work);

    return 0;
};

//Static Method
int SocketListener::handler_quit(const char *UNUSED(path), const char *UNUSED(types), lo_arg ** UNUSED(argv), int UNUSED(argc), void *UNUSED(data), void *user_data){
    SocketListener* object_ref = (SocketListener*) user_data;

    lo_server s = lo_server_thread_get_server(object_ref->thrd_server);

    cout << "Request from port : " << lo_server_get_port(s) << endl;

    return 0;
};

//EOF
