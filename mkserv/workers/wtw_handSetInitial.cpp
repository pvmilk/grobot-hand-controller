/**
 * \file wtw_handSetInitial.cpp
 * \class WTWriteHandSetInitial
 * 
 * \brief Implementation of Class WTWriteHandSetInitial
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtw_handSetInitial.h"

using namespace std;

WTWriteHandSetInitial::WTWriteHandSetInitial(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTWriteHandSetInitial::WTWriteHandSetInitial(const WTWriteHandSetInitial& wt)
    :WThreadOSC(wt){

};

WTWriteHandSetInitial::~WTWriteHandSetInitial(){

};

WTWriteHandSetInitial& WTWriteHandSetInitial::operator=(const WTWriteHandSetInitial& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/handSetInitial
 *  arg  : int[0]   : msg tag
 * */
unsigned WTWriteHandSetInitial::executeThis(){
    handsrv->cb_handSetInitial();

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "i", args->msg_tag);
    list_sock->releaseSockWriteMutex();

    return(0);
}

//EOF
