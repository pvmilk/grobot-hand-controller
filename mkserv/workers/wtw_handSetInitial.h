/**
 * \file wtw_handSetInitial.h
 * \class WTWriteHandSetInitial
 * 
 * \brief Header of Class WTWriteHandSetInitial
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTWriteHandSetInitial_H_
#define _WTWriteHandSetInitial_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTWriteHandSetInitial
 *
 */
class WTWriteHandSetInitial: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTWriteHandSetInitial(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTWriteHandSetInitial(const WTWriteHandSetInitial& wt);

        /// Destructor
        ~WTWriteHandSetInitial();

        /// Operator=
        WTWriteHandSetInitial& operator=(const WTWriteHandSetInitial& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
