/**
 * \file wtr_handGetVoltage.h
 * \class WTReadHandGetVoltage
 * 
 * \brief Header of Class WTReadHandGetVoltage
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTReadHandGetVoltage_H_
#define _WTReadHandGetVoltage_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTReadHandGetVoltage
 *
 */
class WTReadHandGetVoltage: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTReadHandGetVoltage(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTReadHandGetVoltage(const WTReadHandGetVoltage& wt);

        /// Destructor
        ~WTReadHandGetVoltage();

        /// Operator=
        WTReadHandGetVoltage& operator=(const WTReadHandGetVoltage& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
