/**
 * \file wtw_servoOn.cpp
 * \class WTWriteServoOn
 * 
 * \brief Implementation of Class WTWriteServoOn
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtw_servoOn.h"

using namespace std;

WTWriteServoOn::WTWriteServoOn(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTWriteServoOn::WTWriteServoOn(const WTWriteServoOn& wt)
    :WThreadOSC(wt){

};

WTWriteServoOn::~WTWriteServoOn(){

};

WTWriteServoOn& WTWriteServoOn::operator=(const WTWriteServoOn& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/servoOn
 *  arg  : int[0]   : msg tag
 * */
unsigned WTWriteServoOn::executeThis(){
    handsrv->cb_servoOn();

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "i", args->msg_tag);
    list_sock->releaseSockWriteMutex();

    return(0);
}

//EOF
