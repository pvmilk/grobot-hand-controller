/**
 * \file wt_osc.cpp
 * \class WThreadOSC
 * 
 * \brief Implementation of Class WThreadOSC
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>
#include <lo/lo.h>

#include "hand_server.h"
#include "workers/wt_osc.h"

WThreadOSC::WThreadOSC(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WorkerThread(i), handsrv(NULL), list_sock(l_sock){

    ret_addr = lo_address_new_with_proto(LO_UDP, lo_address_get_hostname(address), lo_address_get_port(address));
    handsrv  = list_sock->handsrv_cb;

    /*
     * ADHOC approach for creating args
     * This can be solved by fixing copy constructor of WThreadOSC_ARG 
     * */
    if(const WTJoint_ARG* aj = dynamic_cast<const WTJoint_ARG*>(&arguments)){
        if(const WTJointD_ARG* ajd = dynamic_cast<const WTJointD_ARG*>(aj)){
            this->args = new WTJointD_ARG(*ajd);
        }else{
            this->args = new WTJoint_ARG(*aj);
        }
    }else if(const WTFinger_ARG* af = dynamic_cast<const WTFinger_ARG*>(&arguments)){
        if(const WTFingerD_ARG* afd = dynamic_cast<const WTFingerD_ARG*>(af)){
            this->args = new WTFingerD_ARG(*afd);
        }else{
            this->args = new WTFinger_ARG(*af);
        }
    }else if(const WTThreadOSCD_ARG* at = dynamic_cast<const WTThreadOSCD_ARG*>(&arguments)){
        this->args = new WTThreadOSCD_ARG(*at);
    }else{
        this->args = new WThreadOSC_ARG(arguments);
    }
};

WThreadOSC::WThreadOSC(const WThreadOSC& wt)
    :WorkerThread(wt.id), handsrv(wt.handsrv), list_sock(wt.list_sock), ret_addr(wt.ret_addr), args(NULL){

    /*
     * ADHOC approach for copying args
     * This can be solved by fixing copy constructor of WThreadOSC_ARG 
     * */
    if(WTJoint_ARG* aj = dynamic_cast<WTJoint_ARG*>(wt.args)){
        if(const WTJointD_ARG* ajd = dynamic_cast<const WTJointD_ARG*>(aj)){
            this->args = new WTJointD_ARG(*ajd);
        }else{
            this->args = new WTJoint_ARG(*aj);
        }
    }else if(WTFinger_ARG* af = dynamic_cast<WTFinger_ARG*>(wt.args)){
        if(const WTFingerD_ARG* afd = dynamic_cast<const WTFingerD_ARG*>(af)){
            this->args = new WTFingerD_ARG(*afd);
        }else{
            this->args = new WTFinger_ARG(*af);
        }
    }else if(WTThreadOSCD_ARG* at = dynamic_cast<WTThreadOSCD_ARG*>(wt.args)){
        this->args = new WTThreadOSCD_ARG(*at);
    }else{
        this->args = new WThreadOSC_ARG(*wt.args);
    }

};

WThreadOSC::~WThreadOSC(){
    lo_address_free(ret_addr);

    if(args)
        delete args;
};

WThreadOSC& WThreadOSC::operator=(const WThreadOSC& wt){
    this->id            = wt.id;
    this->handsrv       = wt.handsrv;
    this->list_sock     = wt.list_sock;
    this->ret_addr      = wt.ret_addr;
    this->args          = wt.args;

    return *this;
}

//EOF
