/**
 * \file wtr_getHandDOFs.h
 * \class WTReadGetHandDOFs
 * 
 * \brief Header of Class WTReadGetHandDOFs
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTReadGetHandDOFs_H_
#define _WTReadGetHandDOFs_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTReadGetHandDOFs
 *
 */
class WTReadGetHandDOFs: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTReadGetHandDOFs(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTReadGetHandDOFs(const WTReadGetHandDOFs& wt);

        /// Destructor
        ~WTReadGetHandDOFs();

        /// Operator=
        WTReadGetHandDOFs& operator=(const WTReadGetHandDOFs& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
