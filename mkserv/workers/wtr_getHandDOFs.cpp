/**
 * \file wtr_getHandDOFs.cpp
 * \class WTReadGetHandDOFs
 * 
 * \brief Implementation of Class WTReadGetHandDOFs
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtr_getHandDOFs.h"

using namespace std;

WTReadGetHandDOFs::WTReadGetHandDOFs(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTReadGetHandDOFs::WTReadGetHandDOFs(const WTReadGetHandDOFs& wt)
    :WThreadOSC(wt){

};

WTReadGetHandDOFs::~WTReadGetHandDOFs(){
};

WTReadGetHandDOFs& WTReadGetHandDOFs::operator=(const WTReadGetHandDOFs& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/getHandDOFs
 *  arg  : int[0]   : msg tag
 *         int[1]   : number of dofs of the hand
 * */
unsigned WTReadGetHandDOFs::executeThis(){
    int dofs_num = handsrv->cb_getHandDOFs();

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "ii", args->msg_tag, dofs_num);
    list_sock->releaseSockWriteMutex();

    return(0);
}

//EOF
