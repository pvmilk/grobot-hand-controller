/**
 * \file wtr_getFingerDOFs.cpp
 * \class WTReadGetFingerDOFs
 * 
 * \brief Implementation of Class WTReadGetFingerDOFs
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtr_getFingerDOFs.h"

using namespace std;

WTReadGetFingerDOFs::WTReadGetFingerDOFs(int i, SocketListener *l_sock, const lo_address &address, const WTFinger_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTReadGetFingerDOFs::WTReadGetFingerDOFs(const WTReadGetFingerDOFs& wt)
    :WThreadOSC(wt){

};

WTReadGetFingerDOFs::~WTReadGetFingerDOFs(){
};

WTReadGetFingerDOFs& WTReadGetFingerDOFs::operator=(const WTReadGetFingerDOFs& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/getFingerDOFs
 *  arg  : int[0]   : msg tag
 *         int[1]   : number of dofs of given finger_idx
 * */
unsigned WTReadGetFingerDOFs::executeThis(){
    int dofs_num = handsrv->cb_getFingerDOFs((dynamic_cast<WTFinger_ARG*>(args))->finger_idx);

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "ii", args->msg_tag, dofs_num);
    list_sock->releaseSockWriteMutex();

    return(0);
}

//EOF
