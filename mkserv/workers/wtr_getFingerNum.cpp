/**
 * \file wtr_getFingerNum.cpp
 * \class WTReadGetFingerNum
 * 
 * \brief Implementation of Class WTReadGetFingerNum
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtr_getFingerNum.h"

using namespace std;

WTReadGetFingerNum::WTReadGetFingerNum(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTReadGetFingerNum::WTReadGetFingerNum(const WTReadGetFingerNum& wt)
    :WThreadOSC(wt){

};

WTReadGetFingerNum::~WTReadGetFingerNum(){
};

WTReadGetFingerNum& WTReadGetFingerNum::operator=(const WTReadGetFingerNum& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/getFingerNum
 *  arg  : int[0]   : msg tag
 *         int[1]   : number of finger on the hand
 * */
unsigned WTReadGetFingerNum::executeThis(){
    int finger_num = handsrv->cb_getFingerNum();

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "ii", args->msg_tag, finger_num);
    list_sock->releaseSockWriteMutex();

    return(0);
}

//EOF
