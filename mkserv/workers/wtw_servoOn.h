/**
 * \file wtw_servoOn.h
 * \class WTWriteServoOn
 * 
 * \brief Header of Class WTWriteServoOn
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTWriteServoOn_H_
#define _WTWriteServoOn_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTWriteServoOn
 *
 */
class WTWriteServoOn: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTWriteServoOn(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTWriteServoOn(const WTWriteServoOn& wt);

        /// Destructor
        ~WTWriteServoOn();

        /// Operator=
        WTWriteServoOn& operator=(const WTWriteServoOn& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
