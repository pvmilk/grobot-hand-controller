/**
 * \file wtr_handGetAngle.cpp
 * \class WTReadHandGetAngle
 * 
 * \brief Implementation of Class WTReadHandGetAngle
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtr_handGetAngle.h"

using namespace std;

WTReadHandGetAngle::WTReadHandGetAngle(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTReadHandGetAngle::WTReadHandGetAngle(const WTReadHandGetAngle& wt)
    :WThreadOSC(wt){

};

WTReadHandGetAngle::~WTReadHandGetAngle(){
};

WTReadHandGetAngle& WTReadHandGetAngle::operator=(const WTReadHandGetAngle& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/handGetAngle
 *  arg  : int[0]   : msg tag
 *         blob[1]  : array of bytes
 *         int[2]   : size in bytes of each element of the array
 * */
unsigned WTReadHandGetAngle::executeThis(){
    vector<double> dofs;
    handsrv->cb_handGetAngle(dofs);

    lo_blob a = lo_blob_new(sizeof(double)*dofs.size(), dofs.data());

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "ibi", args->msg_tag, a, sizeof(double));
    list_sock->releaseSockWriteMutex();

    lo_blob_free(a);

    return(0);
}

//EOF
