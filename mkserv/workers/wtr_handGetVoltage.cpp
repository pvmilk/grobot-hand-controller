/**
 * \file wtr_handGetVoltage.cpp
 * \class WTReadHandGetVoltage
 * 
 * \brief Implementation of Class WTReadHandGetVoltage
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtr_handGetVoltage.h"

using namespace std;

WTReadHandGetVoltage::WTReadHandGetVoltage(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTReadHandGetVoltage::WTReadHandGetVoltage(const WTReadHandGetVoltage& wt)
    :WThreadOSC(wt){

};

WTReadHandGetVoltage::~WTReadHandGetVoltage(){
};

WTReadHandGetVoltage& WTReadHandGetVoltage::operator=(const WTReadHandGetVoltage& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/handGetVoltage
 *  arg  : int[0]   : msg tag
 *         blob[1]  : array of bytes
 *         int[2]   : size in bytes of each element of the array
 * */
unsigned WTReadHandGetVoltage::executeThis(){
    vector<double> voltages;
    handsrv->cb_handGetVoltage(voltages);

    lo_blob a = lo_blob_new(sizeof(double)*voltages.size(), voltages.data());

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "ibi", args->msg_tag, a, sizeof(double));
    list_sock->releaseSockWriteMutex();

    lo_blob_free(a);

    return(0);
}

//EOF
