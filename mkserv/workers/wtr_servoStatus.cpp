/**
 * \file wtr_servoStatus.cpp
 * \class WTReadServoStatus
 * 
 * \brief Implementation of Class WTReadServoStatus
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtr_servoStatus.h"

using namespace std;

WTReadServoStatus::WTReadServoStatus(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTReadServoStatus::WTReadServoStatus(const WTReadServoStatus& wt)
    :WThreadOSC(wt){

};

WTReadServoStatus::~WTReadServoStatus(){

};

WTReadServoStatus& WTReadServoStatus::operator=(const WTReadServoStatus& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/servoStatus
 *  arg  : int[0]   : msg tag
 *         int[1]   : servo status; on(1), off(0)
 * */
unsigned WTReadServoStatus::executeThis(){
    int i_status = (handsrv->cb_servoStatus())?1:0;

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "ii", args->msg_tag, i_status);
    list_sock->releaseSockWriteMutex();

    return(0);
}

//EOF
