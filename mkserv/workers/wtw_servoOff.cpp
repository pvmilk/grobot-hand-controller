/**
 * \file wtw_servoOff.cpp
 * \class WTWriteServoOff
 * 
 * \brief Implementation of Class WTWriteServoOff
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtw_servoOff.h"

using namespace std;

WTWriteServoOff::WTWriteServoOff(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTWriteServoOff::WTWriteServoOff(const WTWriteServoOff& wt)
    :WThreadOSC(wt){

};

WTWriteServoOff::~WTWriteServoOff(){

};

WTWriteServoOff& WTWriteServoOff::operator=(const WTWriteServoOff& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/servoOff
 *  arg  : int[0]   : msg tag
 * */
unsigned WTWriteServoOff::executeThis(){
    handsrv->cb_servoOff();

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "i", args->msg_tag);
    list_sock->releaseSockWriteMutex();

    return(0);
}

//EOF
