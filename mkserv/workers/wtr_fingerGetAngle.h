/**
 * \file wtr_fingerGetAngle.h
 * \class WTReadFingerGetAngle
 * 
 * \brief Header of Class WTReadFingerGetAngle
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTReadFingerGetAngle_H_
#define _WTReadFingerGetAngle_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTReadFingerGetAngle
 *
 */
class WTReadFingerGetAngle: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTReadFingerGetAngle(int i, SocketListener *l_sock, const lo_address &address, const WTFinger_ARG &arguments);

        /// Copy Constructor
        WTReadFingerGetAngle(const WTReadFingerGetAngle& wt);

        /// Destructor
        ~WTReadFingerGetAngle();

        /// Operator=
        WTReadFingerGetAngle& operator=(const WTReadFingerGetAngle& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
