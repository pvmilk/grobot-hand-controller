/**
 * \file wtw_fingerSetAngle.h
 * \class WTWriteFingerSetAngle
 * 
 * \brief Header of Class WTWriteFingerSetAngle
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTWriteFingerSetAngle_H_
#define _WTWriteFingerSetAngle_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTWriteFingerSetAngle
 *
 */
class WTWriteFingerSetAngle: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTWriteFingerSetAngle(int i, SocketListener *l_sock, const lo_address &address, const WTFinger_ARG &arguments);

        /// Copy Constructor
        WTWriteFingerSetAngle(const WTWriteFingerSetAngle& wt);

        /// Destructor
        ~WTWriteFingerSetAngle();

        /// Operator=
        WTWriteFingerSetAngle& operator=(const WTWriteFingerSetAngle& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
