/**
 * \file wtw_jointSetAngle.h
 * \class WTWriteJointSetAngle
 * 
 * \brief Header of Class WTWriteJointSetAngle
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTWriteJointSetAngle_H_
#define _WTWriteJointSetAngle_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTWriteJointSetAngle
 *
 */
class WTWriteJointSetAngle: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTWriteJointSetAngle(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTWriteJointSetAngle(const WTWriteJointSetAngle& wt);

        /// Destructor
        ~WTWriteJointSetAngle();

        /// Operator=
        WTWriteJointSetAngle& operator=(const WTWriteJointSetAngle& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
