/**
 * \file wtr_fingerGetAngle.cpp
 * \class WTReadFingerGetAngle
 * 
 * \brief Implementation of Class WTReadFingerGetAngle
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtr_fingerGetAngle.h"

using namespace std;

WTReadFingerGetAngle::WTReadFingerGetAngle(int i, SocketListener *l_sock, const lo_address &address, const WTFinger_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTReadFingerGetAngle::WTReadFingerGetAngle(const WTReadFingerGetAngle& wt)
    :WThreadOSC(wt){

};

WTReadFingerGetAngle::~WTReadFingerGetAngle(){
};

WTReadFingerGetAngle& WTReadFingerGetAngle::operator=(const WTReadFingerGetAngle& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/fingerGetAngle
 *  arg  : int[0]   : msg tag
 *         blob[1]  : array of bytes (angles of the corresponded fingers)
 *         int[2]   : size in bytes of each element of the array
 * */
unsigned WTReadFingerGetAngle::executeThis(){
    vector<double> dofs;
    handsrv->cb_fingerGetAngle((dynamic_cast<WTFinger_ARG*>(args))->finger_idx, dofs);

    lo_blob a = lo_blob_new(sizeof(double)*dofs.size(), dofs.data());

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "ibi", args->msg_tag, a, sizeof(double));
    list_sock->releaseSockWriteMutex();

    lo_blob_free(a);

    return(0);
}

//EOF
