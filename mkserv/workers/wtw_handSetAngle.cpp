/**
 * \file wtw_handSetAngle.cpp
 * \class WTWriteHandSetAngle
 * 
 * \brief Implementation of Class WTWriteHandSetAngle
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtw_handSetAngle.h"

using namespace std;

WTWriteHandSetAngle::WTWriteHandSetAngle(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTWriteHandSetAngle::WTWriteHandSetAngle(const WTWriteHandSetAngle& wt)
    :WThreadOSC(wt){

};

WTWriteHandSetAngle::~WTWriteHandSetAngle(){
};

WTWriteHandSetAngle& WTWriteHandSetAngle::operator=(const WTWriteHandSetAngle& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/write/handSetAngle
 *  arg  : int[0]    : msg tag
 *         int[1]    : whether command is sent to the servos
 * */
unsigned WTWriteHandSetAngle::executeThis(){
    bool isSent = handsrv->cb_handSetAngle(dynamic_cast<WTThreadOSCD_ARG*>(args)->dofs);

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "ii", args->msg_tag, isSent?1:0);
    list_sock->releaseSockWriteMutex();

    return(0);
}

//EOF
