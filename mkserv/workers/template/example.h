/**
 * \file wtr_servoStatus.h
 * \class WTReadServoStatus
 * 
 * \brief Header of Class WTReadServoStatus
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTReadServoStatus_H_
#define _WTReadServoStatus_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTReadServoStatus
 *
 */
class WTReadServoStatus: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTReadServoStatus(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTReadServoStatus(const WTReadServoStatus& wt);

        /// Destructor
        ~WTReadServoStatus();

        /// Operator=
        WTReadServoStatus& operator=(const WTReadServoStatus& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
