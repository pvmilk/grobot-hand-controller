/**
 * \file wtw_handSetAngle.h
 * \class WTWriteHandSetAngle
 * 
 * \brief Header of Class WTWriteHandSetAngle
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTWriteHandSetAngle_H_
#define _WTWriteHandSetAngle_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTWriteHandSetAngle
 *
 */
class WTWriteHandSetAngle: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTWriteHandSetAngle(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTWriteHandSetAngle(const WTWriteHandSetAngle& wt);

        /// Destructor
        ~WTWriteHandSetAngle();

        /// Operator=
        WTWriteHandSetAngle& operator=(const WTWriteHandSetAngle& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
