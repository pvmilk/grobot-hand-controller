/**
 * \file wtw_servoOff.h
 * \class WTWriteServoOff
 * 
 * \brief Header of Class WTWriteServoOff
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTWriteServoOff_H_
#define _WTWriteServoOff_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTWriteServoOff
 *
 */
class WTWriteServoOff: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTWriteServoOff(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTWriteServoOff(const WTWriteServoOff& wt);

        /// Destructor
        ~WTWriteServoOff();

        /// Operator=
        WTWriteServoOff& operator=(const WTWriteServoOff& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
