/**
 * \file wtr_getFingerNum.h
 * \class WTReadGetFingerNum
 * 
 * \brief Header of Class WTReadGetFingerNum
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTReadGetFingerNum_H_
#define _WTReadGetFingerNum_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTReadGetFingerNum
 *
 */
class WTReadGetFingerNum: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTReadGetFingerNum(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTReadGetFingerNum(const WTReadGetFingerNum& wt);

        /// Destructor
        ~WTReadGetFingerNum();

        /// Operator=
        WTReadGetFingerNum& operator=(const WTReadGetFingerNum& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
