/**
 * \file wtr_jointGetAngle.h
 * \class WTReadJointGetAngle
 * 
 * \brief Header of Class WTReadJointGetAngle
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTReadJointGetAngle_H_
#define _WTReadJointGetAngle_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTReadJointGetAngle
 *
 */
class WTReadJointGetAngle: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTReadJointGetAngle(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTReadJointGetAngle(const WTReadJointGetAngle& wt);

        /// Destructor
        ~WTReadJointGetAngle();

        /// Operator=
        WTReadJointGetAngle& operator=(const WTReadJointGetAngle& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
