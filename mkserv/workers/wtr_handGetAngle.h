/**
 * \file wtr_handGetAngle.h
 * \class WTReadHandGetAngle
 * 
 * \brief Header of Class WTReadHandGetAngle
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTReadHandGetAngle_H_
#define _WTReadHandGetAngle_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTReadHandGetAngle
 *
 */
class WTReadHandGetAngle: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTReadHandGetAngle(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WTReadHandGetAngle(const WTReadHandGetAngle& wt);

        /// Destructor
        ~WTReadHandGetAngle();

        /// Operator=
        WTReadHandGetAngle& operator=(const WTReadHandGetAngle& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
