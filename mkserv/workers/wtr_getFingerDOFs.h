/**
 * \file wtr_getFingerDOFs.h
 * \class WTReadGetFingerDOFs
 * 
 * \brief Header of Class WTReadGetFingerDOFs
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WTReadGetFingerDOFs_H_
#define _WTReadGetFingerDOFs_H_

#include "workers/wt_osc.h"

/*!
 *  Class WTReadGetFingerDOFs
 *
 */
class WTReadGetFingerDOFs: public WThreadOSC{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WTReadGetFingerDOFs(int i, SocketListener *l_sock, const lo_address &address, const WTFinger_ARG &arguments);

        /// Copy Constructor
        WTReadGetFingerDOFs(const WTReadGetFingerDOFs& wt);

        /// Destructor
        ~WTReadGetFingerDOFs();

        /// Operator=
        WTReadGetFingerDOFs& operator=(const WTReadGetFingerDOFs& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis();

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
};

#endif//_Example_H_
//EOF
