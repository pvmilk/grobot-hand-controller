/**
 * \file wtr_jointGetAngle.cpp
 * \class WTReadJointGetAngle
 * 
 * \brief Implementation of Class WTReadJointGetAngle
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtr_jointGetAngle.h"

using namespace std;

WTReadJointGetAngle::WTReadJointGetAngle(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTReadJointGetAngle::WTReadJointGetAngle(const WTReadJointGetAngle& wt)
    :WThreadOSC(wt){

};

WTReadJointGetAngle::~WTReadJointGetAngle(){
};

WTReadJointGetAngle& WTReadJointGetAngle::operator=(const WTReadJointGetAngle& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/read/jointGetAngle
 *  arg  : int[0]       : msg tag
 *         double[1]    : angle of the corresponded joint
 * */
unsigned WTReadJointGetAngle::executeThis(){
    WTJoint_ARG* a = dynamic_cast<WTJoint_ARG*>(args);

    double angle = handsrv->cb_jointGetAngle(a->finger_idx, a->joint_idx);

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "id", args->msg_tag, angle);
    list_sock->releaseSockWriteMutex();

    return(0);
};

//EOF
