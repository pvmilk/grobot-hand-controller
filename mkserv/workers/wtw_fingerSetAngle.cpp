/**
 * \file wtw_fingerSetAngle.cpp
 * \class WTWriteFingerSetAngle
 * 
 * \brief Implementation of Class WTWriteFingerSetAngle
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtw_fingerSetAngle.h"

using namespace std;

WTWriteFingerSetAngle::WTWriteFingerSetAngle(int i, SocketListener *l_sock, const lo_address &address, const WTFinger_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTWriteFingerSetAngle::WTWriteFingerSetAngle(const WTWriteFingerSetAngle& wt)
    :WThreadOSC(wt){

};

WTWriteFingerSetAngle::~WTWriteFingerSetAngle(){
};

WTWriteFingerSetAngle& WTWriteFingerSetAngle::operator=(const WTWriteFingerSetAngle& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/write/fingerSetAngle
 *  arg  : int[0]    : msg tag
 *         int[1]    : whether command is sent to the servos
 * */
unsigned WTWriteFingerSetAngle::executeThis(){
    WTFingerD_ARG* a = dynamic_cast<WTFingerD_ARG*>(args);

    bool isSent = handsrv->cb_fingerSetAngle(a->finger_idx, a->dofs);

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "ii", args->msg_tag, isSent?1:0);
    list_sock->releaseSockWriteMutex();

    return(0);
}

//EOF
