/**
 * \file wtw_jointSetAngle.cpp
 * \class WTWriteJointSetAngle
 * 
 * \brief Implementation of Class WTWriteJointSetAngle
 *
 * \author VINAYAVEKHIN Phongtharin
 */
#include <iostream>

#include "hand_server.h"

#include "workers/wt_osc.h"
#include "workers/wtw_jointSetAngle.h"

using namespace std;

WTWriteJointSetAngle::WTWriteJointSetAngle(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments)
    :WThreadOSC(i, l_sock, address, arguments){

};

WTWriteJointSetAngle::WTWriteJointSetAngle(const WTWriteJointSetAngle& wt)
    :WThreadOSC(wt){

};

WTWriteJointSetAngle::~WTWriteJointSetAngle(){
};

WTWriteJointSetAngle& WTWriteJointSetAngle::operator=(const WTWriteJointSetAngle& wt){
    WThreadOSC::operator=(wt);

    return *this;
}

/*
 * RETURN Value:
 *  path : /ans/write/jointSetAngle
 *  arg  : int[0]    : msg tag
 *         int[1]    : whether command is sent to the servos
 * */
unsigned WTWriteJointSetAngle::executeThis(){
    WTJointD_ARG* a = dynamic_cast<WTJointD_ARG*>(args);

    bool isSent = handsrv->cb_jointSetAngle(a->finger_idx, a->joint_idx, a->angle);

    list_sock->lockSockWriteMutex();
    lo_send(ret_addr, ("/ans" + args->path).c_str(), "ii", args->msg_tag, isSent?1:0);
    list_sock->releaseSockWriteMutex();

    return(0);
};

//EOF
