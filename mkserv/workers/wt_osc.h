/**
 * \file wt_osc.h
 * \class WThreadOSC
 * 
 * \brief Header of Class WThreadOSC
 *
 * NOTE : Checkout
 *          void *ThreadPool::threadExecute(void *param)
 *        The objects of these WorkerThreads will be deleted when the execution finished.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _WThreadOSC_H_
#define _WThreadOSC_H_

#include <lo/lo.h>
#include <string>

#include "socket_listener.h"
#include "threadpool.h"

class HandServer;

/*!
 *  Class WThreadOSC
 *
 */
struct WThreadOSC_ARG{
    int msg_tag;        //tag for UDP package tracking
    std::string path;   //Received path

    WThreadOSC_ARG(int t, const char* p):msg_tag(t), path(p){};

    WThreadOSC_ARG(const WThreadOSC_ARG& wta):msg_tag(wta.msg_tag), path(wta.path){};

    virtual ~WThreadOSC_ARG(){};

    WThreadOSC_ARG& operator=(const WThreadOSC_ARG& wta){
        this->msg_tag  = wta.msg_tag;
        this->path = wta.path;
        return *this;
    };
};

struct WTThreadOSCD_ARG: public WThreadOSC_ARG{
    vector<double> dofs;

    WTThreadOSCD_ARG(int t, const char* p, const vector<double> &d)
        :WThreadOSC_ARG(t, p), dofs(d){};

    WTThreadOSCD_ARG(const WTThreadOSCD_ARG &args)
        :WThreadOSC_ARG(args), dofs(args.dofs){}

    virtual ~WTThreadOSCD_ARG(){};

    WTThreadOSCD_ARG& operator=(const WTThreadOSCD_ARG& args){
        WThreadOSC_ARG::operator=(args);
        this->dofs = args.dofs;
        return *this;
    }
};

struct WTFinger_ARG: public WThreadOSC_ARG{
    int finger_idx;

    WTFinger_ARG(int t, const char* p, int f_idx)
        :WThreadOSC_ARG(t, p), finger_idx(f_idx){};

    WTFinger_ARG(const WTFinger_ARG &args)
        :WThreadOSC_ARG(args), finger_idx(args.finger_idx){}

    virtual ~WTFinger_ARG(){};

    WTFinger_ARG& operator=(const WTFinger_ARG& args){
        WThreadOSC_ARG::operator=(args);
        this->finger_idx = args.finger_idx;
        return *this;
    }
};

struct WTFingerD_ARG: public WTFinger_ARG{
    vector<double> dofs;

    WTFingerD_ARG(int t, const char* p, int f_idx, vector<double> &d)
        :WTFinger_ARG(t, p, f_idx), dofs(d){};

    WTFingerD_ARG(const WTFingerD_ARG &args)
        :WTFinger_ARG(args), dofs(args.dofs){}

    virtual ~WTFingerD_ARG(){};

    WTFingerD_ARG& operator=(const WTFingerD_ARG& args){
        WTFinger_ARG::operator=(args);
        this->dofs = args.dofs;
        return *this;
    }
};

struct WTJoint_ARG: public WTFinger_ARG{
    int joint_idx;

    WTJoint_ARG(int t, const char* p, int f_idx, int j_idx)
        :WTFinger_ARG(t, p, f_idx), joint_idx(j_idx){};

    WTJoint_ARG(const WTJoint_ARG &args)
        :WTFinger_ARG(args), joint_idx(args.joint_idx){}

    virtual ~WTJoint_ARG(){};

    WTJoint_ARG& operator=(const WTJoint_ARG& args){
        WTFinger_ARG::operator=(args);
        this->joint_idx = args.joint_idx;
        return *this;
    }
};

struct WTJointD_ARG: public WTJoint_ARG{
    double angle;

    WTJointD_ARG(int t, const char* p, int f_idx, int j_idx, double a)
        :WTJoint_ARG(t, p, f_idx, j_idx), angle(a){};

    WTJointD_ARG(const WTJointD_ARG &args)
        :WTJoint_ARG(args), angle(args.angle){}

    ~WTJointD_ARG(){};

    WTJointD_ARG& operator=(const WTJointD_ARG& args){
        WTJoint_ARG::operator=(args);
        this->angle = args.angle;
        return *this;
    }
};

class WThreadOSC : public WorkerThread{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        WThreadOSC(int i, SocketListener *l_sock, const lo_address &address, const WThreadOSC_ARG &arguments);

        /// Copy Constructor
        WThreadOSC(const WThreadOSC& wt);

        /// Destructor
        virtual ~WThreadOSC();

        /// Operator=
        WThreadOSC& operator=(const WThreadOSC& wt);

        /// This will be executed with the thread is assigned.
        unsigned virtual executeThis() = 0;

    ///////////////////////////// VARIABLES /////////////////////////////
    protected:
        SocketListener* list_sock;      // Pointer to 
        lo_address      ret_addr;       // Information about returning address
        HandServer*     handsrv;        // Pointer to HandServer callback
        WThreadOSC_ARG* args;           // Pointer to argument of the function call
};

#endif//_Example_H_
//EOF
