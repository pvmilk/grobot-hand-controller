/**
 * \file socket_listener.h
 * \class SocketListener
 * 
 * \brief Header of Class SocketListener
 *
 * SocketListener : This implementation will allow one HandServer to listen to multiple ports.
 *
 * \author VINAYAVEKHIN Phongtharin
 */

#ifndef _SocketListener_H_
#define _SocketListener_H_

#include <lo/lo.h>
#include <unistd.h>
class HandServer;
class WThreadOSC;

/*!
 *  Class SocketListener
 *
 */
class SocketListener{
    friend class WThreadOSC;

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        SocketListener(const char* port, HandServer* cb);

        /// Copy Constructor
        SocketListener(const SocketListener& sl);

        /// Destructor
        ~SocketListener();

        /// Operator=
        SocketListener& operator=(const SocketListener& sl);

        void start();

        void lockSockWriteMutex();

        void releaseSockWriteMutex();

    private:
        SocketListener* getSelfReference();

        static void handler_sock_error(int num, const char *msg, const char *path);

        static int  handler_write_servoon(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_write_servooff(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_read_servostatus(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_write_handsetinitial(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_write_handsetangle(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_read_handgetangle(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_read_handgetvoltage(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_write_fingersetangle(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_read_fingergetangle(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_write_jointsetangle(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_read_jointgetangle(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_read_gethanddofs(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_read_getfingernum(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_read_getfingerdofs(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

        static int  handler_quit(const char *path, const char *types, lo_arg ** argv, int argc, void *data, void *user_data);

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
        pthread_mutex_t  lock_mu_write_sock;               // MUTEX Locks for writing to socket (for sending msg)
        lo_server_thread thrd_server;

        HandServer* handsrv_cb;

        static unsigned int latest_wid;
};

#endif//_SocketListener_H_
//EOF
