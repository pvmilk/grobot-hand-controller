TARGET           = hand-osc-server
TEMPLATE         = app
CONFIG          -= qt   #exclude the linkage to -lQtGUI and -lQtCore

INCLUDEPATH     += ../include ./

OBJECTS_DIR     += .obj

LIBS            += -L"../lib/" -lfutabahand -llo -lyaml-cpp

QMAKE_CXXFLAGS  +=-g -std=c++0x

HEADERS         += hand_server.h \
                   socket_listener.h \
                   threadpool.h \
                   workers/wt_osc.h \
                   workers/wtw_servoOn.h \
                   workers/wtw_servoOff.h \
                   workers/wtr_servoStatus.h \
                   workers/wtw_handSetInitial.h \
                   workers/wtw_handSetAngle.h \
                   workers/wtr_handGetAngle.h \
                   workers/wtr_handGetVoltage.h \
                   workers/wtw_fingerSetAngle.h \
                   workers/wtr_fingerGetAngle.h \
                   workers/wtw_jointSetAngle.h \
                   workers/wtr_jointGetAngle.h \
                   workers/wtr_getHandDOFs.h \
                   workers/wtr_getFingerNum.h \
                   workers/wtr_getFingerDOFs.h

SOURCES         += main.cpp \
                   hand_server.cpp \
                   socket_listener.cpp \
                   threadpool.cpp \
                   workers/wt_osc.cpp \
                   workers/wtw_servoOn.cpp \
                   workers/wtw_servoOff.cpp \
                   workers/wtr_servoStatus.cpp \
                   workers/wtw_handSetInitial.cpp \
                   workers/wtw_handSetAngle.cpp \
                   workers/wtr_handGetAngle.cpp \
                   workers/wtr_handGetVoltage.cpp \
                   workers/wtw_fingerSetAngle.cpp \
                   workers/wtr_fingerGetAngle.cpp \
                   workers/wtw_jointSetAngle.cpp \
                   workers/wtr_jointGetAngle.cpp \
                   workers/wtr_getHandDOFs.cpp \
                   workers/wtr_getFingerNum.cpp \
                   workers/wtr_getFingerDOFs.cpp

