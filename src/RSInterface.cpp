/**
 * \file RSInterface.cpp
 * \class RSInterface
 * 
 * \brief Implementation of Class RSInterface
 *
 * \author VINAYAVEKHIN Phongtharin; pv-milk@cvl.iis.u-tokyo.ac.jp
 */

#include "RSInterface.h"

#include <assert.h>
#include <cstring>
#include <climits>
#include <fcntl.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <termios.h>

using namespace std;

RSInterface::RSInterface(const char* port){
    //Verify that this class will act correctly in this machine.
    verify_machine();

    //Open COM_PORT
    ttys_fd = open(port, O_RDWR | O_NOCTTY | O_NDELAY);
    if (ttys_fd == -1){
        perror("RSInterface: Unable to open COM_PORT");
        throw;
    }else{
        fcntl(ttys_fd, F_SETFL, 0);
        cout << "RSInterface: COM_PORT " << port << " has been sucessfully opened." << endl;
    }

    //Save original setting of COM_PORT
    tcgetattr(ttys_fd, &original_setting);

    //Configure COM_PORT
    configure_port();
};

RSInterface::RSInterface(const RSInterface& rsi){
    (void)rsi;      /// Get rid of compiler's unused warning
};

RSInterface::~RSInterface(){
    //Resume original setting of COM_PORT
    tcsetattr(ttys_fd, TCSANOW, &original_setting);

    //Close COM_PORT
    close(ttys_fd);
};

RSInterface& RSInterface::operator=(const RSInterface& rsi){
    (void)rsi;      /// Get rid of compiler's unused warning
    return *this;
}

void RSInterface::verify_machine(){
    assert(sizeof(unsigned char) == 1);
    assert(CHAR_BIT == 8);
};

/**
  * Reference :
  *  - http://www.tek-tips.com/viewthread.cfm?qid=924030
  *  - http://www.comptechdoc.org/os/linux/programming/c/linux_pgcserial.html
  *  - http://stackoverflow.com/questions/7610103/is-this-linux-serial-port-configured-the-same-as-the-windows-one
  *  - http://stackoverflow.com/questions/7791621/looking-for-test-program-for-linux-serial-port
  *
  *  - settings : http://www.unixguide.net/unix/programming/3.6.2.shtml
  * */
void RSInterface::configure_port(){
    struct termios new_settings;

    //Initialize struct of setting to 0
    memset(&new_settings, 0, sizeof(new_settings));


    /*  baudrate(115200), bytesize(8bits), parity_bit(no parity check), stop_bit(1)
     *
     *      B115200     : baudrate 
     *      CS8         : 8n1 (8 bits, no parity, stop_bit 1)
     *      CLOCAL      : local setting, no modem control
     *      CREAD       : receiving characters
     */
    new_settings.c_cflag = B115200 | CS8 | CREAD | CLOCAL;

    // set input mode
    // $BFC$K2?$b@_Dj$7$J$$(B
    new_settings.c_iflag = 0;

    // set output mode to RAW MODE
    // $BFC$K2?$b@_Dj$7$J$$(B = Raw $B%b!<%I(B ($B=PNO%G!<%?$K2?$b<j$r2C$($J$$(B)$B$G=PNO(B
    new_settings.c_oflag = 0;

    // set local mode to non-canonical mode
    // $BFC$K2?$b@_Dj$7$J$$(B = $BHs%+%N%K%+%k%b!<%I(B
    new_settings.c_lflag = 0;

    /* set timeout
     *
     * read() will return when VMIN bytes of input are available, or if interrupted.
     * Otherwise it will wait indefinitely
     */
    new_settings.c_cc[VTIME]    = 0;
    new_settings.c_cc[VMIN]     = 1;

    //Flush existing data
    tcflush(ttys_fd, TCIFLUSH);

    //Apply new setting to COM_PORT
    tcsetattr(ttys_fd, TCSANOW, &new_settings);
};

/**
  * @param (integer)  id                        servo's id
  * @param (integer)  flag                      flag
  * @param (integer)  adr                       starting address which data will be written
  * @param (integer)  data_length               length of data
  * @param (integer*) data[data_length]         data to be sent
  *
  * @return bool                                whether the data is succesfully sent
  * */
bool RSInterface::send_shortpacket(unsigned char id, unsigned char flag, unsigned char adr, unsigned char data_length, unsigned char* data){
    int buf_length = 8+data_length;
    unsigned char buf[buf_length];
    
    buf[0] = 0xFA;              /// Header 1st byte
    buf[1] = 0xAF;              /// Header 2nd byte
    buf[2] = id;                /// servo's is
    buf[3] = flag;              /// flag
    buf[4] = adr;               /// address on memory map
    buf[5] = data_length;       /// length of data
    buf[6] = 0x01;              /// Number of servos (always 1 for short packet)
    for(int i = 0; i < data_length; i++){
        buf[7+i] = data[i];
    }

    /// 'sum' is a value obtained from XOR operation on all bytes from ID through Data
    unsigned char sum = buf[2]; 
    for(int i = 3; i < 7+data_length; i++){
        sum = sum ^ buf[i];     /// bitwised XOR
    }
    buf[7+data_length]  = sum;  /// Check sum

    //Flush existing data
    tcflush(ttys_fd, TCIFLUSH);

    //Write to COM_PORT
    ssize_t ret = write(ttys_fd, &buf, buf_length);

    //PRINT TO DEBUG
    //std::cout << "send_shortpacket : " << std::hex;
    //for (int i = 0; i < buf_length; i++){
    //    std::cout << (int) buf[i] << " ";
    //}
    //std::cout << std::endl;

    return !(ret == -1);
};


/**
  * @param (integer)  id                        servo's id
  * @param (integer)  flag                      flag
  * @param (integer)  adr                       starting address which data will be written
  * @param (integer)  length                    length to send
  *
  * @return bool                                whether the data is succesfully sent
  * */
bool RSInterface::send_shortpacket_nodata(unsigned char id, unsigned char flag, unsigned char adr, unsigned char length, unsigned char cnt){
    int buf_length = 8;
    unsigned char buf[buf_length];
    
    buf[0] = 0xFA;              /// Header 1st byte
    buf[1] = 0xAF;              /// Header 2nd byte
    buf[2] = id;                /// servo's is
    buf[3] = flag;              /// flag
    buf[4] = adr;               /// address on memory map
    buf[5] = length;            /// length of data
    buf[6] = cnt;               /// Cnt

    /// 'sum' is a value obtained from XOR operation on all bytes from ID through Data
    unsigned char sum = buf[2]; 
    for(int i = 3; i < 7; i++){
        sum = sum ^ buf[i];     /// bitwised XOR
    }
    buf[7]  = sum;  /// Check sum

    //Flush existing data
    tcflush(ttys_fd, TCIFLUSH);

    //Write to COM_PORT
    ssize_t ret = write(ttys_fd, &buf, buf_length);

    //PRINT TO DEBUG
    //std::cout << "send_shortpacket : " << std::hex;
    //for (int i = 0; i < buf_length; i++){
    //    std::cout << (int) buf[i] << " ";
    //}
    //std::cout << std::endl;

    return !(ret == -1);
};



/**
  * @param (integer)   adr                      starting address which data will be written
  * @param (integer)   count                    number of servos which data will be sent to
  * @param (integer*)  id[count]                list of servos' id
  * @param (integer)   data_length              length of data  (length of each data MUST be same)
  * @param (integer**) data[count][data_length] data to be sent
  *
  * @return bool                                whether the data is succesfully sent
  * */
bool RSInterface::send_longpacket( unsigned char adr, unsigned char count, unsigned char* id, unsigned char data_length, unsigned char** data){
    int buf_length = 8 + count*(data_length+1);
    unsigned char buf[buf_length];
    buf[0] = 0xFA;              /// Header 1st byte
    buf[1] = 0xAF;              /// Header 2nd byte
    buf[2] = 0x00;              /// Always 0 for Long Packet
    buf[3] = 0x00;              /// Always 0 for Long Packet
    buf[4] = adr;               /// address on memory map
    buf[5] = data_length + 1;   /// length of data + 1 (byte of VID)
    buf[6] = count;             /// Number of servos

    for(int k = 0; k < count; k++){
        int start_idx           = 7+ k*(data_length+1);
        buf[start_idx]          = id[k];
        for(int i = 0; i < data_length; i++){
            buf[start_idx+1+i]  = data[k][i];
        }
    }
    
    /// 'sum' is a value obtained from XOR operation on all bytes from ID through Data
    unsigned char sum = buf[2]; 
    for(int i = 3; i < 7 + count*(data_length+1); i++){
        sum = sum ^ buf[i];     /// bitwised XOR
    }
    buf[7 + count*(data_length+1)]  = sum;  /// Check sum

    //Flush existing data
    tcflush(ttys_fd, TCIFLUSH);

    //Write to COM_PORT
    ssize_t ret = write(ttys_fd, &buf, buf_length);

    //PRINT TO DEBUG
    //std::cout << "send_longpacket : " << std::hex;
    //for (int i = 0; i < buf_length; i++){
    //    std::cout << (int) buf[i] << " ";
    //}
    //std::cout << std::endl;

    return !(ret == -1);
};

/**
  * @param id           servo's id
  * @param param[30]     return parameters
  *
  * @return int     >= 0 : The result is ok
  *                  < 0 : Error
  *
  * NOTE : Refer to the use of 'pass by reference' for array 
  *         http://stackoverflow.com/questions/9509829/c-size-of-array
  * */
int RSInterface::get_param_0to29(unsigned char id, unsigned char (&param)[30]){
    int param_len  = 30;
    unsigned char data_length       = 0x00;
    unsigned char data[data_length];
    
    /// Set flag to '0 0 1 1' (0x03)
    int w = send_shortpacket((unsigned char) id, (unsigned char) 0x03, (unsigned char)0x00, data_length, data);
    if (w == 0){
        return -1;                  /// Error : send data fail
    }

    /// Wait before fetch result
    usleep(50000);  // 50ms

    /// Get return data
    int rbuf_len = 8+param_len;
    unsigned char rbuf[rbuf_len];
    int n = read(ttys_fd, &rbuf, rbuf_len);
    
    if ( n < rbuf_len){
        return -2;                  /// Error : reading data fail
    }
    
    /// Verify with check-sum
    unsigned char sum = rbuf[2];
    for (int i =3; i < rbuf_len; i++){
        sum = sum ^ rbuf[i];      /// rbuf[rbuf_len-1] is check-sum anyway.
    }
    if (sum){
        return -3;                  /// Error : check-sum fail
    }

    /// Copy data
    memcpy(&param[0], &rbuf[7], param_len*sizeof(unsigned char));

    return 0;
};



/**
  * @param id           servo's id
  * @param param[12]     return parameters
  *
  * @return int     >= 0 : The result is ok
  *                  < 0 : Error
  *
  * NOTE : Refer to the use of 'pass by reference' for array 
  *         http://stackoverflow.com/questions/9509829/c-size-of-array
  * */
int RSInterface::get_param_30to41(unsigned char id, unsigned char (&param)[12]){
    int param_len  = 12;
    unsigned char data_length       = 0x00;
    unsigned char data[data_length];
    
    /// Set flag to '1 0 1 1' (0x0B)
    int w = send_shortpacket((unsigned char) id, (unsigned char) 0x0B, (unsigned char)0x00, data_length, data);
    if (w == 0){
        return -1;                  /// Error : send data fail
    }

    /// Wait before fetch result
    usleep(50000);  // 50ms

    /// Get return data
    int rbuf_len = 8+param_len;
    unsigned char rbuf[rbuf_len];
    int n = read(ttys_fd, &rbuf, rbuf_len);
    
    if ( n < rbuf_len){
        return -2;                  /// Error : reading data fail
    }
    
    /// Verify with check-sum
    unsigned char sum = rbuf[2];
    for (int i =3; i < rbuf_len; i++){
        sum = sum ^ rbuf[i];      /// rbuf[rbuf_len-1] is check-sum anyway.
    }
    if (sum){
        return -3;                  /// Error : check-sum fail
    }

    /// Copy data
    memcpy(&param[0], &rbuf[7], param_len*sizeof(unsigned char));

    return 0;
};


/**
  * @param id           servo's id
  * @param param[18]     return parameters
  *
  * @return int     >= 0 : The result is ok
  *                  < 0 : Error
  *
  * NOTE : Refer to the use of 'pass by reference' for array 
  *         http://stackoverflow.com/questions/9509829/c-size-of-array
  * */
int RSInterface::get_param_42to59(unsigned char id, unsigned char (&param)[18]){
    int param_len  = 18;
    unsigned char data_length       = 0x00;
    unsigned char data[data_length];
    
    /// Set flag to '1 0 0 1' (0x09)
    int w = send_shortpacket((unsigned char) id, (unsigned char) 0x09, (unsigned char)0x00, data_length, data);
    if (w == 0){
        return -1;                  /// Error : send data fail
    }

    /// Wait before fetch result
    usleep(50000);  // 50ms

    /// Get return data
    int rbuf_len = 8+param_len;
    unsigned char rbuf[rbuf_len];
    int n = read(ttys_fd, &rbuf, rbuf_len);
    
    if ( n < rbuf_len){
        return -2;                  /// Error : reading data fail
    }
    
    /// Verify with check-sum
    unsigned char sum = rbuf[2];
    for (int i =3; i < rbuf_len; i++){
        sum = sum ^ rbuf[i];      /// rbuf[rbuf_len-1] is check-sum anyway.
    }
    if (sum){
        return -3;                  /// Error : check-sum fail
    }

    /// Copy data
    memcpy(&param[0], &rbuf[7], param_len*sizeof(unsigned char));

    return 0;
};


/**
  * @param id       servo's id
  * @param mode     0 : turn off servo
  *                 1 : turn on servo
  *
  * */
void RSInterface::torque_onoff(const unsigned int id, bool on){
    unsigned char data_length       = 0x01;
    unsigned char data[data_length];
    data[0]                         = (on)? 0x01:0x00;
    
    send_shortpacket((unsigned char) id, (unsigned char) 0x00, (unsigned char)0x24, data_length, data);
};

/** 
  * @param cnt      lenght of 'id'
  * @param id*      list of servos' id
  * @param mode     0 : turn off servo
  *                 1 : turn on servo
  *
  * */
void RSInterface::torque_onoff(const unsigned int cnt, const unsigned int* id, bool on){
    unsigned char data_length   = 0x01;
    unsigned char* data[cnt];
    unsigned char cid[cnt];
    
    for(unsigned int k = 0; k < cnt; k++){
        data[k]     = new unsigned char[data_length];
        data[k][0]  = (on)? 0x01:0x00;
        cid[k]      = (unsigned char) id[k];
    }
    send_longpacket((unsigned char) 0x24, (unsigned char) cnt, cid, data_length, data);
    
    for(unsigned int k = 0; k < cnt; k++){
        delete data[k];
    }
};

/**
  * @param id       servo's id
  *
  * @return int     0 : servo is disable (off)
  *                 1 : servo is enable (on)
  *                 2 : servo is in brake mode
  *
  * */
int RSInterface::torque_status(const unsigned int id){
    unsigned char readparam[12];
    int n = get_param_30to41((unsigned char)id, readparam);
    if(n!=0){
        cout << "RSInterface::torque_status : get_param_30to41 return " << n << endl;
        assert(n==0);
    }

    //mm[36]    Torque Enable
    return (int)readparam[36-30];
};

/**
  * @param id       servo's id
  * @param reversed 0 : normal direction 
  *                 1 : reversed direction
  *
  * @return bool    0 : unsuccessfully set, if the torque is ON. (Nothing altered)
  *                 1 : successfully set
  * */
bool RSInterface::set_rotate_dir(const unsigned int id, bool reversed){
    if (torque_status(id) != 0)
        return 0;

    unsigned char data_length       = 0x01;
    unsigned char data[data_length];
    data[0]                         = (reversed)? 0x01:0x00;
    
    send_shortpacket((unsigned char) id, (unsigned char) 0x00, (unsigned char)0x05, data_length, data);

    return 1;
};

/** 
  * @param cnt      lenght of 'id'
  * @param id*      list of servos' id
  * @param reversed 0 : normal direction 
  *                 1 : reversed direction
  *
  * @return bool    0 : unsuccessfully set, if one of the torque is ON. (Nothing altered)
  *                 1 : successfully set
  * */
bool RSInterface::set_rotate_dir(const unsigned int cnt, const unsigned int* id, bool reversed){
    for(unsigned int k = 0; k < cnt; k++){
        if (torque_status(id[k]) !=0)
            return 0;
    }

    unsigned char data_length   = 0x01;
    unsigned char* data[cnt];
    unsigned char cid[cnt];
    
    for(unsigned int k = 0; k < cnt; k++){
        data[k]     = new unsigned char[data_length];
        data[k][0]  = (reversed)? 0x01:0x00;
        cid[k]      = (unsigned char) id[k];
    }
    send_longpacket((unsigned char) 0x05, (unsigned char) cnt, cid, data_length, data);
    
    for(unsigned int k = 0; k < cnt; k++){
        delete data[k];
    }

    return 1;
};

/**
  * @param id       servo's id
  * @param cw       0 : set for CW direction
  *                 1 : set for CCW direction
  * @param slope    compliance_slope [0, 255]; higher value --> more flexible
  *
  *  NOTE : CW/CCW direction will follow set_rotate_dir, or in other word
  *             CW  for reaction-force in +angle direction when consider set_rotate_dir into account.
  *             CCW for reaction-force in -angle direction when consider set_rotate_dir into account.
  * */
void RSInterface::set_compliance_slope(const unsigned int id, bool cw, unsigned char slope){
    unsigned char data_length       = 0x01;
    unsigned char data[data_length];
    data[0]                         = slope;
    
    if (cw)
        send_shortpacket((unsigned char) id, (unsigned char) 0x00, (unsigned char)0x1A, data_length, data);
    else
        send_shortpacket((unsigned char) id, (unsigned char) 0x00, (unsigned char)0x1B, data_length, data);

};

/** 
  * @param cnt      lenght of 'id'
  * @param id*      list of servos' id
  * @param cw       0 : set for CW direction
  *                 1 : set for CCW direction
  * @param slope}   compliance_slope [0, 255]; higher value --> more flexible
  *
  *             NOTE : cnt == sizeof(id) == sizeof(slope)
  *
  *  NOTE : CW/CCW direction will follow set_rotate_dir, or in other word
  *             CW  for reaction-force in +angle direction when consider set_rotate_dir into account.
  *             CCW for reaction-force in -angle direction when consider set_rotate_dir into account.
  * */
void RSInterface::set_compliance_slope(const unsigned int cnt, const unsigned int* id, bool cw, const unsigned char *slope){
    unsigned char data_length   = 0x01;
    unsigned char* data[cnt];
    unsigned char cid[cnt];
    
    for(unsigned int k = 0; k < cnt; k++){
        data[k]     = new unsigned char[data_length];
        data[k][0]  = slope[k];
        cid[k]      = (unsigned char) id[k];
    }
    if (cw)
        send_longpacket((unsigned char) 0x1A, (unsigned char) cnt, cid, data_length, data);
    else
        send_longpacket((unsigned char) 0x1B, (unsigned char) cnt, cid, data_length, data);
    
    for(unsigned int k = 0; k < cnt; k++){
        delete data[k];
    }
};


double dRound(double r){
    return (r > 0.0) ? floor(r+0.5) : ceil(r-0.5);
};

/**
  * @param id       servo's id
  * @param angle    angle to move joint to, in degree
  *                 ... precision upto 1/10 of a degree (e.g. 50.568 degree will be considered as 50.6 degree)
  *                     (feasible range is [-150, 150])
  *
  * @return bool    whether the command is executed
  *
  * */
bool RSInterface::move(const unsigned int id, double angle){
    if(angle < -150.0 || angle > 150.0){
        return false;
    }
    short goal_angle            = (short) dRound(angle*10);

    unsigned char data_length   = 0x04;
    unsigned char data[data_length];
    data[0] = goal_angle&0x00FF;                /// Goal Position L
    data[1] = (goal_angle&0xFF00)>>8;           /// Goal Position H

    data[2] = 0x00;                             /// Goal Time L (AS FAST AS POSSIBLE)
    data[3] = 0x00;                             /// Goal Time H (AS FAST AS POSSIBLE)
    
    send_shortpacket((unsigned char) id, (unsigned char) 0x00, (unsigned char)0x1E, data_length, data);

    return true;
};

/** 
  * @param cnt      lenght of 'id'
  * @param id*      list of servos' id
  * @param angle*   list of angle to move joint to, in degree
  *                 ... precision upto 1/10 of a degree (e.g. 50.568 degree will be considered as 50.6 degree)
  *                     (feasible range is [-150, 150])
  *
  *             NOTE : cnt == sizeof(id) == sizeof(angle)
  *
  * @return bool    whether the command is executed
  *
  * */
bool RSInterface::move(unsigned int cnt, const unsigned int* id, const double* angle){
    /// Check 'angle' and assign to 'goal_angle'
    short goal_angle[cnt];
    for(unsigned int k = 0; k < cnt; k++){
        if(angle[k] < -150.0 || angle[k] > 150.0){
            return false;
        }
        goal_angle[k]           = (short) dRound(angle[k]*10);
    }
    
    unsigned char data_length     = 0x04;
    unsigned char* data[cnt];
    unsigned char cid[cnt];
    
    for(unsigned int k =0; k < cnt; k++){
        data[k]     = new unsigned char[data_length];
        data[k][0]  = goal_angle[k]&0x00FF;                /// Goal Position L
        data[k][1]  = (goal_angle[k]&0xFF00)>>8;           /// Goal Position H
        
        data[k][2]  = 0x00;                                /// Goal Time L (AS FAST AS POSSIBLE)
        data[k][3]  = 0x00;                                /// Goal Time H (AS FAST AS POSSIBLE)
        cid[k]      = (unsigned char) id[k];
    }
    
    send_longpacket((unsigned char) 0x1E, (unsigned char) cnt, cid, data_length, data);

    for(unsigned int k = 0; k < cnt; k++){
        delete data[k];
    }
    
    return true;
};


/**
  * @param id       servo's id
  * @param angle    angle to move joint to, in degree
  *                 ... precision upto 1/10 of a degree (e.g. 50.568 degree will be considered as 50.6 degree)
  *                     (feasible range is [-150, 150])
  *
  * @param speed    speed to move joint to a specific angle, in degree/sec
  *                 ... in the case that the speed is faster than the maximum speed of the servo, the servo moves with its maximum speed.
  *                     (maximum speed for RS303MR 0.11sec/60 degree ~= 545.454545 degree/sec)
  *
  * @return bool    whether the command is executed
  *
  * NOTE :  'GOAL TIME' can be set upto 1/100 of a second of precision (e.g.  10.5678 seconds will be considred as 10.57 seconds)
  *                     (feasible range of 'GOAL TIME' [0, 163.83] second)
  * */
bool RSInterface::move(const unsigned int id, double angle, double speed){
    if(angle < -150.0 || angle > 150.0){
        return false;
    }
    short goal_angle            = (short) dRound(angle*10);

    if(speed < 0){
        return false;
    }

    double cangle               = getAngle(id);
    double time                 = fabs((angle-cangle)/speed);
    double dgoal_time           = dRound(time*100);
    unsigned short  goal_time   = (dgoal_time - 16383 > 1e-6)? 16383: (unsigned short) dgoal_time;

    unsigned char data_length   = 0x04;
    unsigned char data[data_length];
    data[0] = goal_angle&0x00FF;                /// Goal Position L
    data[1] = (goal_angle&0xFF00)>>8;           /// Goal Position H

    data[2] = goal_time&0x00FF;                 /// Goal Time L
    data[3] = (goal_time&0xFF00)>>8;            /// Goal Time H
    
    send_shortpacket((unsigned char) id, (unsigned char) 0x00, (unsigned char)0x1E, data_length, data);

    return true;
};


/** 
  * @param cnt      lenght of 'id'
  * @param id*      list of servos' id
  * @param angle*   list of angle to move joint to, in degree
  *                 ... precision upto 1/10 of a degree (e.g. 50.568 degree will be considered as 50.6 degree)
  *                     (feasible range is [-150, 150])
  * @param speed    list of speed to move joint to a specific angle, in degree/sec
  *                 ... in the case that the speed is faster than the maximum speed of the servo, the servo moves with its maximum speed.
  *                     (maximum speed for RS303MR 0.11sec/60 degree ~= 545.454545 degree/sec)
  *
  *             NOTE : cnt == sizeof(id) == sizeof(angle) == sizeof(speed)
  *
  * @return bool    whether the command is executed
  *
  * NOTE :  'GOAL TIME' can be set upto 1/100 of a second of precision (e.g.  10.5678 seconds will be considred as 10.57 seconds)
  *                     (feasible range of 'GOAL TIME' [0, 163.83] second)
  *
  * */
bool RSInterface::move(unsigned int cnt, const unsigned int* id, const double* angle, const double* speed){
    /// Check 'angle'&'speed' and assign to 'goal_angle'&'goal_speed'
    short goal_angle[cnt];
    short goal_time[cnt];
    for(unsigned int k = 0; k < cnt; k++){
        if(angle[k] < -150.0 || angle[k] > 150.0){
            return false;
        }
        goal_angle[k]           = (short) dRound(angle[k]*10);
        
        if(speed[k] < 0){
            return false;
        }
        
        double cangle               = getAngle(id[k]);
        double time                 = fabs((angle[k]-cangle)/speed[k]);
        double dgoal_time           = dRound(time*100);
        goal_time[k]                = (dgoal_time - 16383 > 1e-6)? 16383: (unsigned short) dgoal_time;
    }
    
    unsigned char data_length     = 0x04;
    unsigned char* data[cnt];
    unsigned char cid[cnt];
    
    for(unsigned int k =0; k < cnt; k++){
        data[k]     = new unsigned char[data_length];
        data[k][0]  = goal_angle[k]&0x00FF;                /// Goal Position L
        data[k][1]  = (goal_angle[k]&0xFF00)>>8;           /// Goal Position H
        
        data[k][2] = goal_time[k]&0x00FF;                 /// Goal Time L
        data[k][3] = (goal_time[k]&0xFF00)>>8;            /// Goal Time H
        cid[k]      = (unsigned char) id[k];
    }
    
    send_longpacket((unsigned char) 0x1E, (unsigned char) cnt, cid, data_length, data);

    for(unsigned int k = 0; k < cnt; k++){
        delete data[k];
    }
    
    return true;
};

void RSInterface::getCondition(const unsigned int id, Condition &cond){
    int n = get_param_42to59((unsigned char)id, (unsigned char (&)[18]) *cond.param.data());
    if(n!=0){
        cout << "RSInterface::getCondition : get_param_42to59 return " << n << endl;
        assert(n==0);
    }
};

/**
  * @param id       servo's id
  *
  * @return double  current angle, in degree
  *
  * */
double RSInterface::getAngle(const unsigned int id){
    Condition cond;
    getCondition(id, cond);

    return cond.getAngle();
};


/**
  * @param id       servo's id
  *
  * @return double  Elapsed time after a servo receives a packet to move, in msec
  *
  * Note: If the "Goal Time" of the movement is 0x00, this Elapsed time will not be rewritten (maintain last value)
  *
  * */
double RSInterface::getElapsedTime(const unsigned int id){
    Condition cond;
    getCondition(id, cond);

    return cond.getElapsedTime();
};


/**
  * @param id       servo's id
  *
  * @return double  current speed, in degree/second
  *
  * */
double RSInterface::getSpeed(const unsigned int id){
    Condition cond;
    getCondition(id, cond);

    return cond.getSpeed();
};

/**
  * @param id       servo's id
  *
  * @return bool    0 : servo is in normal mode
  *                 1 : servo is in 'reversed' mode
  *
  * */
bool RSInterface::getRotateDir(const unsigned int id){
    unsigned char readparam[30];
    int n = get_param_0to29((unsigned char)id, readparam);
    if(n!=0){
        cout << "RSInterface::getRotateDir : get_param_0to29 return " << n << endl;
        assert(n==0);
    }

    //mm[5]    Reverse
    return (int)readparam[5-0];
};

/**
  * @param id           servo's id
  * @param param[60]     return parameters
  *
  * @return int     >= 0 : The result is ok
  *                  < 0 : Error
  *
  * NOTE : Refer to the use of 'pass by reference' for array 
  *         http://stackoverflow.com/questions/9509829/c-size-of-array
  * */
int RSInterface::get_param_mm(unsigned char id, unsigned char (&param)[60]){
    int param_len  = 60;
    unsigned char length       = 0x3C;       // 60 Bytes starting from address 0x00
    
    /// Set flag to '1 1 1 1' (0x0F)
    int w = send_shortpacket_nodata((unsigned char) id, (unsigned char) 0x0F, (unsigned char)0x00, length, (unsigned char)0x00);
    if (w == 0){
        return -1;                  /// Error : send data fail
    }

    /// Wait before fetch result
    usleep(50000);  // 50ms

    /// Get return data
    int rbuf_len = 8+param_len;
    unsigned char rbuf[rbuf_len];
    int n = read(ttys_fd, &rbuf, rbuf_len);
    
    if ( n < rbuf_len){
        return -2;                  /// Error : reading data fail
    }
    
    /// Verify with check-sum
    unsigned char sum = rbuf[2];
    for (int i =3; i < rbuf_len; i++){
        sum = sum ^ rbuf[i];      /// rbuf[rbuf_len-1] is check-sum anyway.
    }
    if (sum){
        return -3;                  /// Error : check-sum fail
    }

    /// Copy data
    memcpy(&param[0], &rbuf[7], param_len*sizeof(unsigned char));

    return 0;
};

/**
  * @param id       servo's id
  *
  * @return         servo's id
  *
  * */
unsigned int RSInterface::getServoID(const unsigned int id){
    unsigned char readparam[30];
    int n = get_param_0to29((unsigned char)id, readparam);
    if(n!=0){
        cout << "RSInterface::getServoID : get_param_0to29 return " << n << endl;
        assert(n==0);
    }

    //mm[4]    Servo ID
    return (unsigned int)readparam[4-0];
};

/**
  * @param  prev_id       previous servo's id
  *         new_id        new servo's id
  *
  * @return -1              invalid new id
  *          0              attempt to change the id is not success
  *          1              id is changed to the new one, and flash to the ROM
  *          2              id is changed to the new one, but isn't flash to the ROM, will return to previous one once turn off.
  * */
int RSInterface::set_servo_id(const unsigned int prev_id, const unsigned int new_id){
    if (new_id < 1 or new_id > 128){
        return -1;
    }

    unsigned char data_length       = 0x01;
    unsigned char data[data_length];
    data[0]                         = (unsigned char) new_id;

    bool changeSuccess = send_shortpacket((unsigned char) prev_id, (unsigned char) 0x00, (unsigned char)0x04, data_length, data);
    if (!changeSuccess){
        return 0;
    }else{
        bool flashSuccess = send_shortpacket_nodata((unsigned char) new_id, (unsigned char) 0x40, (unsigned char) 0xFF, (unsigned char) 0x00, (unsigned char) 0x00);
        if(flashSuccess){
            return 1;
        }else{
            return 2;
        }
    }
}

//EOF
