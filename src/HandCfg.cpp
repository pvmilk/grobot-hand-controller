/**
 * \file HandCfg.cpp
 * \class HandCfg
 * 
 * \brief Implementation of Class HandCfg
 *
 * \author VINAYAVEKHIN Phongtharin; pv-milk@cvl.iis.u-tokyo.ac.jp
 */

#include <assert.h>
#include <fstream>
#include <iostream>
#include <yaml-cpp/yaml.h>

#include "HandCfg.h"

using namespace std;
///////////////////////////////// Class HandCfg ////////////////////////////////

void operator >> (const YAML::Node& node, vector<int>& vec){
    for(unsigned int i = 0; i < node.size(); i++){
        int i_temp = -1;
        node[i] >> i_temp;
        vec.push_back(i_temp);
    }
};

void operator >> (const YAML::Node& node, vector<double>& vec){
    for(unsigned int i = 0; i < node.size(); i++){
        double d_temp = -1;
        node[i] >> d_temp;
        vec.push_back(d_temp);
    }
};

HandCfg::HandCfg(std::string cfg_filename){
    ifstream fin(cfg_filename.c_str());

    // Initialize parser
    YAML::Parser parser(fin);
    YAML::Node doc;
    parser.GetNextDocument(doc);
    const YAML::Node& node = doc[0];

    //// Start parsing
    node["tty_port"]                >> tty_port;
    node["num_of_dofs"]             >> num_of_dofs;
    node["num_of_fingers"]          >> num_of_fingers;
    node["num_of_jointsPERfingers"] >> num_of_jPf;
    node["map_joint2servoID"]       >> j2sid;
    node["joint_home"]              >> joint_home;
    node["joint_speed"]             >> joint_speed;

    vector<unsigned int> temp_compliant_cw;
    vector<unsigned int> temp_compliant_ccw;
    node["compliant_cw"]            >> temp_compliant_cw;
    node["compliant_ccw"]           >> temp_compliant_ccw;

    //// Make sure the configuration file is sane.

    //num_of_dofs
    assert(num_of_dofs > 0);

    //num_of_fingers
    assert(num_of_fingers > 0);

    //num_of_jPf
    assert(num_of_jPf.size() == num_of_fingers);
    unsigned all_dofs = 0;
    for(unsigned int i = 0; i < num_of_jPf.size(); i++){
        assert(num_of_jPf[i] > 0);
        all_dofs += num_of_jPf[i];
    }
    assert(all_dofs == num_of_dofs);

    // j2sid
    assert(j2sid.size() == num_of_dofs);
    for(unsigned int i = 0; i < j2sid.size(); i++){
        assert(j2sid[i] >=0 && j2sid[i] < 128);
    }

    // joint_home
    assert(joint_home.size() == num_of_dofs);

    // joint_speed
    assert(joint_speed.size() == num_of_dofs);

    // temp_compliant_cw
    assert(temp_compliant_cw.size() == num_of_dofs);
    for(unsigned int i = 0; i < temp_compliant_cw.size(); i++){
        assert(temp_compliant_cw[i] >=0 && temp_compliant_cw[i] < 256);
    }

    // compliant_ccw
    assert(temp_compliant_ccw.size() == num_of_dofs);
    for(unsigned int i = 0; i < temp_compliant_ccw.size(); i++){
        assert(temp_compliant_ccw[i] >=0 && temp_compliant_ccw[i] < 256);
    }

    // Copy and cast temp_compliant_cw|temp_compliant_ccw to compliant_cw|compliant_ccw
    compliant_cw.resize(temp_compliant_cw.size());
    compliant_ccw.resize(temp_compliant_ccw.size());
    for(unsigned int i = 0; i < num_of_dofs; i++){
        compliant_cw[i]  = (unsigned char)temp_compliant_cw[i];
        compliant_ccw[i] = (unsigned char)temp_compliant_ccw[i];
    }

    // Initialize startDOF_4finger
    unsigned int c_idx = 0;
    for(unsigned int i = 0; i < num_of_fingers; i++){
        startDOF_4finger[i] = c_idx;
        c_idx += num_of_jPf[i];
    }

};

HandCfg::HandCfg(const HandCfg& cfg){
    tty_port         = cfg.tty_port;
    num_of_dofs      = cfg.num_of_dofs;
    num_of_fingers   = cfg.num_of_fingers;
    num_of_jPf       = cfg.num_of_jPf;
    j2sid            = cfg.j2sid;
    joint_home       = cfg.joint_home;
    joint_speed      = cfg.joint_speed;
    compliant_cw     = cfg.compliant_cw;
    compliant_ccw    = cfg.compliant_ccw;
    startDOF_4finger = cfg.startDOF_4finger;
};

HandCfg::~HandCfg(){

};

HandCfg& HandCfg::operator=(const HandCfg& cfg){
    this->tty_port         = cfg.tty_port;
    this->num_of_dofs      = cfg.num_of_dofs;
    this->num_of_fingers   = cfg.num_of_fingers;
    this->num_of_jPf       = cfg.num_of_jPf;
    this->j2sid            = cfg.j2sid;
    this->joint_home       = cfg.joint_home;
    this->joint_speed      = cfg.joint_speed;
    this->compliant_cw     = cfg.compliant_cw;
    this->compliant_ccw    = cfg.compliant_ccw;
    this->startDOF_4finger = cfg.startDOF_4finger;
    return *this;
};

