/**
 * \file FutabaHand.cpp
 * \class FutabaHand
 * 
 * \brief Implementation of Class FutabaHand
 *
 * \author VINAYAVEKHIN Phongtharin; pv-milk@cvl.iis.u-tokyo.ac.jp
 */

#include "FutabaHand.h"

#include "RSInterface.h"
#include "HandCfg.h"
#include <assert.h>
#include <iostream>
#include <map>
#include <vector>

using namespace std;

FutabaHand::FutabaHand(const char* cfg_filename)
    :hand_conf(string(cfg_filename)), RS_PORT(hand_conf.tty_port.c_str()){

    /*
     * Set Rotation value
     */
    bool dir_reversed = setJointsRotateDir(true);
    usleep(10000);             //10ms

    // Entering this state when one or more servos are ON, and we cannot set rotating direction.
    if (!dir_reversed){
        for (unsigned int k = 0; k < getHandDOFs(); k++){
            int id = hand_conf.j2sid[k];
            if (!RS_PORT.getRotateDir(id)){
                cout << "FutabaHand : At least, one servo motor have strange rotating direction.";
                cout << "Please turn OFF torqe and start again !! "<< endl;
                break;
            }
        }
    }
    usleep(10000);             //10ms

    /*
     * Set Compliance on both CW and CCW direction.
     */
    RS_PORT.set_compliance_slope(getHandDOFs(), hand_conf.j2sid.data(), true, hand_conf.compliant_cw.data() );
    RS_PORT.set_compliance_slope(getHandDOFs(), hand_conf.j2sid.data(), false, hand_conf.compliant_ccw.data() );
    usleep(10000);             //10ms
};

/// Destructor
FutabaHand::~FutabaHand(){

};

/// Copy Constructor
FutabaHand::FutabaHand(const FutabaHand& fh)
    :hand_conf(fh.hand_conf), RS_PORT(hand_conf.tty_port.c_str()){
};

/// Operator=
FutabaHand& FutabaHand::operator=(const FutabaHand& fh){
    (void)fh;       /// Get rid of compiler's unused warning
    return *this;
};

/**
  * @return bool    0 : unsuccessfully set, if one of the torque is ON. (Nothing altered)
  *                 1 : successfully set
  * */
bool FutabaHand::setJointsRotateDir(bool reversed){
    return RS_PORT.set_rotate_dir(getHandDOFs(), hand_conf.j2sid.data(), reversed);
};

void FutabaHand::servoOn(){
    RS_PORT.torque_onoff(getHandDOFs(), hand_conf.j2sid.data(), true);
};

void FutabaHand::servoOff(){
    RS_PORT.torque_onoff(getHandDOFs(), hand_conf.j2sid.data(), false);
};


/**
  * @return bool      0 : One or more server are in Disable mode or Brake mode
  *                   1 : All servos are in Enabled mode.
  * */
bool FutabaHand::servoStatus(){
    for (unsigned int k = 0; k < getHandDOFs(); k++){
        int id = hand_conf.j2sid[k];
        int status = RS_PORT.torque_status(id);
        if (status != 1){
            return false;
        }
    }
    return true;
};

void FutabaHand::handSetInitial(){
    bool ret = RS_PORT.move(getHandDOFs(), hand_conf.j2sid.data(), hand_conf.joint_home.data(), hand_conf.joint_speed.data());
    assert(ret);
};


/**
  * @param  dofs[20]   DOFs value to be set at each joint
  *
  * @return bool      0 : No command is sent to servos
  *                   1 : Command is sent to servos (NOT A GUARANTEE FOR CORRECT RESULT)
  * */
bool FutabaHand::handSetAngle(vector<double> &dofs){
    assert(dofs.size() == getHandDOFs());
    bool ret = RS_PORT.move(getHandDOFs(), hand_conf.j2sid.data(), dofs.data(), hand_conf.joint_speed.data());
    return ret;
};


/**
  * @param dofs     Return value will be stored here
  *                 (Size of the return vector would be equal to getHandDOFs())
  *
  * */
void FutabaHand::handGetAngle(vector<double> &dofs){
    dofs.resize(getHandDOFs());
    for(unsigned int k = 0; k < getHandDOFs(); k++){
        dofs[k] = RS_PORT.getAngle(hand_conf.j2sid[k]);
    }
};


/**
  * @param conditions    Return value will be stored here
  *                      (Size of the return vector would be equal to getHandDOFs())
  *
  * */
void FutabaHand::handGetCondition(vector<Condition> &conditions){
    conditions.resize(getHandDOFs());
    for(unsigned int k = 0; k < getHandDOFs(); k++){
        RS_PORT.getCondition(hand_conf.j2sid[k], conditions[k]);
    }
};


/**
  * @param  dofs[getFingerDOFs()]   DOFs value to be set at each joint of specific finger
  *
  * @return bool                   -1 : Incorrect finger_idx; No cmmand is sent to servos
  *                                 0 : No command is sent to servos
  *                                 1 : Command is sent to servos (NOT A GUARANTEE FOR CORRECT RESULT)
  * */
bool FutabaHand::fingerSetAngle(unsigned int finger_idx, vector<double> &dofs){
    if(finger_idx >= hand_conf.num_of_fingers){
        return -1;
    }

    unsigned int j_num  = getFingerDOFs(finger_idx);
    assert(dofs.size() == j_num);
    unsigned int finger_start_idx = hand_conf.startDOF_4finger[finger_idx];
    bool ret = RS_PORT.move(j_num, &hand_conf.j2sid[finger_start_idx], dofs.data(), &hand_conf.joint_speed[finger_start_idx]);
    return ret;
};


/**
  * @param dofs     Return value will be stored here
  *                 (Size of the return vector would be equal to getFingerDOFs())
  *
  *                 if incorrect finger_idx is given, 'dofs' will not be altered.
  *
  * */
void FutabaHand::fingerGetAngle(unsigned int finger_idx, vector<double> &dofs){
    if(finger_idx < hand_conf.num_of_fingers){
        dofs.resize(getFingerDOFs(finger_idx));
        unsigned int finger_start_idx = hand_conf.startDOF_4finger[finger_idx];
        for(unsigned int k = 0; k < getFingerDOFs(finger_idx); k++){
            dofs[k] = RS_PORT.getAngle(hand_conf.j2sid[finger_start_idx + k]);
        }
    }
};

bool FutabaHand::jointSetAngle(unsigned int finger_idx, unsigned int joint_idx, double dof){
    if(finger_idx < hand_conf.num_of_fingers && joint_idx < hand_conf.num_of_jPf[finger_idx]){
        unsigned int dof_idx = hand_conf.startDOF_4finger[finger_idx]+joint_idx;
        unsigned int joint_id = hand_conf.j2sid[dof_idx];
        bool ret = RS_PORT.move(joint_id, dof, hand_conf.joint_speed[dof_idx]);
        return ret;
    }else{
        return false;
    }
};

double FutabaHand::jointGetAngle(unsigned int finger_idx, unsigned int joint_idx){
    assert(finger_idx < hand_conf.num_of_fingers && joint_idx < hand_conf.num_of_jPf[finger_idx]);
    unsigned int joint_id = hand_conf.j2sid[hand_conf.startDOF_4finger[finger_idx]+joint_idx];
    return RS_PORT.getAngle(joint_id);
};

unsigned int FutabaHand::getHandDOFs(){
    return hand_conf.num_of_dofs;
};

unsigned int FutabaHand::getFingerNum(){
    return hand_conf.num_of_fingers;
};

unsigned int FutabaHand::getFingerDOFs(unsigned int finger_idx){
    return (finger_idx < hand_conf.num_of_fingers)? hand_conf.num_of_jPf[finger_idx]:-1;
};

