TARGET = futabahand
TEMPLATE = lib

CONFIG += staticlib object_with_source

DESTDIR = ../lib/

INCLUDEPATH += ../include

DEPENDPATH += ../src ../include

OBJECTS_DIR += .obj

LIBS            += -lyaml-cpp

QMAKE_CXXFLAGS  +=-g -std=c++0x

#
# FUTABA_HAND LIB
#

SOURCES +=  ../src/RSInterface.cpp\
            ../src/FutabaHand.cpp\
            ../src/HandCfg.cpp

HEADERS +=  ../include/misc.h\
            ../include/RSInterface.h\
            ../include/FutabaHand.h\
            ../include/HandCfg.h


