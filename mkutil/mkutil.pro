TARGET           = grobot-util
TEMPLATE         = app
CONFIG          -= qt   #exclude the linkage to -lQtGUI and -lQtCore

INCLUDEPATH     += ../include ./

OBJECTS_DIR     += .obj

LIBS            += -L"../lib/" -lfutabahand 

QMAKE_CXXFLAGS  +=-g -std=c++0x

SOURCES         +=  main.cpp

