#include <iostream>
#include <stdlib.h>

#include "RSInterface.h"

using namespace std;

int main(int argc, char* argv[]){
    // Change the only connected servo to the following ID
    if (argc != 2){
        cerr << "./grobot-util NEW_ID" << endl;
        return 0;
    }

    int new_id = atoi(argv[1]);
    if(new_id < 1 or new_id > 127){
        cerr << "NEW_ID must be between 1-127" << endl;
        return 0;
    }

    RSInterface rsi;

    cout << "Connect only ONE servo to the hub; and press any key..." << endl;
    cin.ignore();

    int err_code = rsi.set_servo_id(255, new_id);

    if(err_code == 1 or err_code == 2){
        cout << "SUCCESSFULLY change the servo to the NEW_ID=" << new_id << endl;
        if(err_code == 1){
            cout << "ROM Flashed; NEW_ID will reflected even after unplugged the power." << endl;
        }

        // Test
        usleep(1000000);  // 500ms
        cout << endl <<  "Let's test..." << endl;
        rsi.torque_onoff(new_id, true);
        cout << "Servo ON" << endl;
        usleep(1000000);  // 500ms

        rsi.move(new_id, 15);
        cout << "Move" << endl;
        usleep(1000000);  // 500ms

        rsi.torque_onoff(new_id, false);
        cout << "Servo OFF" << endl;
    }else{
        cout << "FAIL to change the servo to the NEW_ID=" << new_id << endl;
    }

    return 0;
};

