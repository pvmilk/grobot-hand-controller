/**
 * \file FutabaHand.h
 * \class FutabaHand
 * 
 * \brief Header of Class FutabaHand
 *
 * \author VINAYAVEKHIN Phongtharin; pv-milk@cvl.iis.u-tokyo.ac.jp
 */

#ifndef _FutabaHand_H_
#define _FutabaHand_H_

#include "RSInterface.h"
#include "HandCfg.h"
#include "misc.h"

#include <map>
#include <vector>

using namespace std;

/*!
 *  Class FutabaHand
 *
 */
class FutabaHand{
    public:
        /// Constructor 
        FutabaHand(const char* cfg_filename);

        /// Destructor
        ~FutabaHand();

    private:
        /// Copy Constructor        --> Disable
        FutabaHand(const FutabaHand& fh);

        /// Operator=               --> Disable
        FutabaHand& operator=(const FutabaHand& fh);

        /// Set Rotate direction of the joint (Which direction to be positive value)
        bool setJointsRotateDir(bool reversed);

    public:
        /// Constructor 
        /// Turn ALL servos on
        void servoOn();

        /// Turn ALL servos off
        void servoOff();

        /// Get current (ALL) servos status (either on|off)
        bool servoStatus();

        ///////////////////////////////////////////////////////////

        /// Move ALL servos to their initial angles (angle = 0)
        void handSetInitial();

        /// Move ALL servos to specific positions; in degree
        bool handSetAngle(vector<double> &dofs);

        /// Get ALL servos current positions; in degree
        void handGetAngle(vector<double> &dofs);

        /// Get ALL servos current condition; (Get as a bulk);
        void handGetCondition(vector<Condition> &conditions);

        ///////////////////////////////////////////////////////////

        /// Move specific finger to specific position
        bool fingerSetAngle(unsigned int finger_idx, vector<double> &dofs);

        /// Get current servos' position for specific finger
        void fingerGetAngle(unsigned int finger_idx, vector<double> &dofs);

        ///////////////////////////////////////////////////////////

        /// Move specific joint to specific position
        bool jointSetAngle(unsigned int finger_idx, unsigned int joint_idx, double dof);

        /// Get current servo's position for specific joint
        double jointGetAngle(unsigned int finger_idx, unsigned int joint_idx);

        ///////////////////////////////////////////////////////////

        /// Get number of DOFs of the hand.
        unsigned int getHandDOFs();

        /// Get number of finger
        unsigned int getFingerNum();

        /// Get number of DOFs of each finger.
        unsigned int getFingerDOFs(unsigned int finger_idx);

    private:
        HandCfg     hand_conf;
        RSInterface RS_PORT;
};

#endif//_FutabaHand_H_
//EOF
