/**
 * \file HandCfg.h
 * \class HandCfg
 * 
 * \brief Header of Class HandCfg
 *
 * \author VINAYAVEKHIN Phongtharin; pv-milk@cvl.iis.u-tokyo.ac.jp
 */

#ifndef _handcfg_H_
#define _handcfg_H_

#include <string>
#include <vector>
#include <map>

class HandCfg{
    public:
        std::string                 tty_port;
        unsigned int                num_of_dofs;
        unsigned int                num_of_fingers;
        
        /// Joint per finger
        std::vector<unsigned int>   num_of_jPf;

        /// Array stores infos to convert from 'Finger'&'Joint' to Servos' id
        std::vector<unsigned int>   j2sid;

        /// Array stores infos of home position of every joint
        std::vector<double>         joint_home;

        /// Array stored infos of default joint speed for every joints; in degree/second
        std::vector<double>         joint_speed;

        /// Array stores infos of default complaint in cw direction for each servos; 
        /// higher value --> more flexible --> less resistence force in angle+ direction
        std::vector<unsigned char>  compliant_cw;

        /// Array stores infos of default complaint in cw direction for each servos
        /// higher value --> more flexible --> less resistence force in angle- direction
        std::vector<unsigned char>  compliant_ccw;

        /// Map stored 'key' and 'value' between finger_idx& the starting idx of the 1st joint
        std::map<unsigned int, unsigned int> startDOF_4finger;

    public:
        HandCfg(std::string cfg_filename);

        HandCfg(const HandCfg& cfg);

        ~HandCfg();

        HandCfg& operator=(const HandCfg& cfg);
};

#endif
