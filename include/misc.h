/**
 * \file misc.h
 *
 * \author VINAYAVEKHIN Phongtharin; pv-milk@cvl.iis.u-tokyo.ac.jp
 */

#ifndef _MISC_H_
#define _MISC_H_

#include <array>
#include <math.h>

/*
 * Condition is used to store raw data between mm[42] to mm[59] for now.
 *
 * It will be designed to be able to change easily, in case other attributes are required.
 * This is because we would like to fetching least data as possible, to trade for speed.
 *
 * */
struct Condition{
    friend class RSInterface;

    private :
        static const int ADDR_FS = 42;  // Fetch start address
        static const int ADDR_FE = 59;  // Fetch end address

        static const int ADDR_POS_L  = 42;   // Present Position L
        static const int ADDR_POS_H  = 43;   // Present Position H
        static const int ADDR_TIME_L = 44;   // Present Time L
        static const int ADDR_TIME_H = 45;   // Present Time H
        static const int ADDR_SPD_L  = 46;   // Present Speed L
        static const int ADDR_SPD_H  = 47;   // Present Speed H
        static const int ADDR_CUR_L  = 48;   // Present Current L
        static const int ADDR_CUR_H  = 49;   // Present Current H
        static const int ADDR_TEMP_L = 50;   // Present Temperature L
        static const int ADDR_TEMP_H = 51;   // Present Temperature H
        static const int ADDR_VOLT_L = 52;   // Present Volts L
        static const int ADDR_VOLT_H = 53;   // Present Volts H

    private :
        std::array<unsigned char, ADDR_FE - ADDR_FS + 1 > param;

    public :
        /// Constructor
        Condition(){};

        /// Copy Constructor
        Condition(const Condition& con)
            :param(con.param){};

        /// Destructor
        ~Condition(){};

        /// Operator=
        Condition& operator=(const Condition& con){
            this->param = con.param;
            return *this;
        };

        /// Get angle of a servo, in degree
        double getAngle(){
            short angle = ((param[ADDR_POS_H - ADDR_FS] << 8) & 0x0000FF00) | (param[ADDR_POS_L - ADDR_FS] & 0x000000FF);
            return angle/10.0;
        };

        /// Get Elapsed time after a servo receives a packet to move, in msec
        double getElapsedTime(){
            short etime = ((param[ADDR_TIME_H - ADDR_FS] << 8) & 0x0000FF00) | (param[ADDR_TIME_L - ADDR_FS] & 0x000000FF);
            return etime*10.0;
        };

        /// Get speed of a servo, in degree/second
        double getSpeed(){
            short speed = ((param[ADDR_SPD_H - ADDR_FS] << 8) & 0x0000FF00) | (param[ADDR_SPD_L - ADDR_FS] & 0x000000FF);
            return fabs(speed);
        };

        /// Get current of a servo, in mA
        double getCurrent(){
            short current = ((param[ADDR_CUR_H - ADDR_FS] << 8) & 0x0000FF00) | (param[ADDR_CUR_L - ADDR_FS] & 0x000000FF);
            return fabs(current);
        };

        /// Get temperature of a servo, in degree celcius
        double getTemperature(){
            short temperature = ((param[ADDR_TEMP_H - ADDR_FS] << 8) & 0x0000FF00) | (param[ADDR_TEMP_L - ADDR_FS] & 0x000000FF);
            return temperature;
        }

        /// Get voltage of a servo, in mV
        double getVoltage(){
            short voltage = ((param[ADDR_VOLT_H - ADDR_FS] << 8) & 0x0000FF00) | (param[ADDR_VOLT_L - ADDR_FS] & 0x000000FF);
            return voltage*10.0;
        }

};

#endif//_MISC_H_
//EOF
