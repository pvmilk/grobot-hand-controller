/**
 * \file RSInterface.h
 * \class RSInterface
 * 
 * \brief Header of Class RSInterface
 *
 * \author VINAYAVEKHIN Phongtharin; pv-milk@cvl.iis.u-tokyo.ac.jp
 */

#ifndef _RSInterface_H_
#define _RSInterface_H_

#include <termios.h>            // POSIX terminal control definitions

#include "misc.h"

/*!
 *  Class RSInterface
 *
 */
class RSInterface{

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        RSInterface(const char* port = "/dev/ttyUSB0");

        /// Destructor
        ~RSInterface();

    private:
        /// Copy Constructor    --> Disable
        RSInterface(const RSInterface& rsi);

        /// Operator=           --> Disable
        RSInterface& operator=(const RSInterface& rsi);

        /// Verify that this class will work correctly with this machine
        ///     Check sizeof bit, byte etc.
        void verify_machine();

        /// Configure COM_PORT for 'Fubata RS303MR Servo'
        void configure_port();

        /// Send short packet (refer to fubata RS303MR/RS304MD Manual)
        bool send_shortpacket(unsigned char id, unsigned char flag, unsigned char adr, unsigned char data_length, unsigned char* data);

        /// Send short packet with out data section (refer to fubata RS303MR/RS304MD Manual)
        bool send_shortpacket_nodata(unsigned char id, unsigned char flag, unsigned char adr, unsigned char length, unsigned char cnt);

        /// Send long packet (refer to fubata RS303MR/RS304MD Manual)
        bool send_longpacket( unsigned char adr, unsigned char count, unsigned char* id, unsigned char data_length, unsigned char** data);

        /// Get the data of memory map No.0 - No.59
        int get_param_mm(unsigned char id, unsigned char (&param)[60]);

        /// Get the data of memory map No.0 - No.29
        int get_param_0to29(unsigned char id, unsigned char (&param)[30]);

        /// Get the data of memory map No.30 - No.41
        int get_param_30to41(unsigned char id, unsigned char (&param)[12]);

        /// Get the data of memory map No.42 - No.59
        int get_param_42to59(unsigned char id, unsigned char (&param)[18]);

    public:
        /// Turn on/off servo's torque
        void torque_onoff(const unsigned int id, bool on);

        /// Simultaneously turn on/off servo's torque
        void torque_onoff(const unsigned int cnt, const unsigned int* id, bool on);

        /// Get servo's status
        int torque_status(const unsigned int id);

        /// Set rotate direction of the servo
        ///
        /// Value changes to initial (no reversed) when unplugged
        /// Can only be set when servo's torque is OFF
        bool set_rotate_dir(const unsigned int id, bool reversed);

        /// Set rotate direction of many servos
        ///
        /// Value changes to initial (no reversed) when unplugged
        /// Can only be set when servos' torque are OFF
        bool set_rotate_dir(const unsigned int cnt, const unsigned int* id, bool reversed);

        /// Set servo's compliance slope; settable range (0-255)... higher -> more flexible
        ///
        /// Value changes to initial (no reversed) when unplugged
        /// cw/ccw direction will follow set_rotate_dir
        void set_compliance_slope(const unsigned int id, bool cw, unsigned char slope);

        /// Set servos' compliance slope; settable range (0-255)... higher -> more flexible
        ///
        /// Value changes to initial (no reversed) when unplugged
        void set_compliance_slope(const unsigned int cnt, const unsigned int* id, bool cw, const unsigned char *slope);

        /// Move servo to specific angle (as fast as possible)
        bool move(const unsigned int id, double angle);

        /// Simultaneously move multiple servos to specific angle (as fast as possible)
        bool move(unsigned int cnt, const unsigned int* id, const double* angle);

        /// Move servo to specific angle in specific speed
        bool move(const unsigned int id, double angle, double speed);

        /// Simultaneously move multiple servos to specific angle in specific speed
        bool move(unsigned int cnt, const unsigned int* id, const double* angle, const double* speed);

        /// Get 'Condition' of a servo
        void getCondition(const unsigned int id, Condition &cond);

        /// Get current angle of a servo, in degree
        double getAngle(const unsigned int id);

        /// Get Elapsed time after a servo receives a packet to move, in msec
        double getElapsedTime(const unsigned int id);

        /// Get current speed of a servo, in degree/second
        double getSpeed(const unsigned int id);

        /// Get current rotate direction of a servo
        bool getRotateDir(const unsigned int id);

        /// Get ID of a servo
        unsigned int getServoID(const unsigned int id);

        void restoreMemoryMap(const unsigned int id = 255);

        /// Set ID of a servo
        ///
        /// If prev_id = 0xFF (255) is used, it will effect to all connected servo
        int set_servo_id(const unsigned int prev_id, const unsigned int new_id);

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
        int ttys_fd;                             /// File descriptor of the port
        struct termios original_setting;         /// Original setting of the COM_PORT

};

#endif//_RSInterface_H_
//EOF
